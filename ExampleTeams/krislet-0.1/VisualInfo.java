//
//	File:			VisualInfo.java
//	Author:		Krzysztof Langner
//	Date:			1997/04/28
//


import java.util.*;


class VisualInfo 
{
//===========================================================================
// Initialization member functions
	public VisualInfo(String info)
	{
		info.trim();
//System.out.println(info);
		m_tokenizer = new StringTokenizer(info,"() ", true);
		m_objects = new Vector(41);
	}

	//---------------------------------------------------------------------------
	// This function parses visual information from the server

	public void parse()
	{
		String token = null;
		ObjectInfo	objInfo;

		try
		{
			// First is '('
			m_tokenizer.nextToken();	
			// then we should find see
			if( m_tokenizer.nextToken().compareTo("see") != 0 )	
				return;

			// now ' ' followed by time and ' '
			m_tokenizer.nextToken();	
			m_time = Integer.parseInt( m_tokenizer.nextToken() );
			m_tokenizer.nextToken();	

			// then we should fine '('
			token = m_tokenizer.nextToken();

			while(token.compareTo("(") == 0)
			{
				// we have another object so go on
				// and get its name.
				objInfo = createNewObject();
				m_objects.addElement(objInfo);
				
				token = m_tokenizer.nextToken();
				if( token.compareTo(")") == 0 )
				{
					token = m_tokenizer.nextToken();
					if(token.compareTo(" ") == 0)
						token = m_tokenizer.nextToken();
					continue;
				}

				// get distance
				token = m_tokenizer.nextToken();
				objInfo.m_distance = Float.valueOf(token).floatValue();
				token = m_tokenizer.nextToken();
				if( token.compareTo(")") == 0 )
				{
					token = m_tokenizer.nextToken();
					if(token.compareTo(" ") == 0)
						token = m_tokenizer.nextToken();
					continue;
				}

				// get direction
				objInfo.m_direction = Float.valueOf(m_tokenizer.nextToken()).floatValue();
				token = m_tokenizer.nextToken();
				if( token.compareTo(")") == 0 )
				{
					token = m_tokenizer.nextToken();
					if(token.compareTo(" ") == 0)
						token = m_tokenizer.nextToken();
					continue;
				}

				// get distance change
				objInfo.m_distChange = Float.valueOf(m_tokenizer.nextToken()).floatValue();
				token = m_tokenizer.nextToken();
				if( token.compareTo(")") == 0 )
				{
					token = m_tokenizer.nextToken();
					if(token.compareTo(" ") == 0)
						token = m_tokenizer.nextToken();
					continue;
				}

				// get direction change
				objInfo.m_dirChange = Float.valueOf(m_tokenizer.nextToken()).floatValue();
				token = m_tokenizer.nextToken();
				token = m_tokenizer.nextToken();
				if(token.compareTo(" ") == 0)
					token = m_tokenizer.nextToken();
			}
		}
		catch(Exception e)
		{
			System.out.println("Error parsing see information");
			System.out.println(token);
			while( m_tokenizer.hasMoreTokens() )
				System.out.print(m_tokenizer.nextToken());

			System.out.println("");
		}
	}



//===========================================================================
// Private implementaions

	//---------------------------------------------------------------------------
	// This function creates new informstion object
	private ObjectInfo createNewObject()
	{
		String			token;
		ObjectInfo	objInfo = null;
		
			// bypass first "("
			m_tokenizer.nextToken();
			token = m_tokenizer.nextToken();
			if(token.compareTo("player") == 0)
			{
				String team = new String();
				int uniformNumber = 0;
				token = m_tokenizer.nextToken();	// space
				if(token.compareTo(" ") == 0)
				{
					team = m_tokenizer.nextToken();	// teamname
					token = m_tokenizer.nextToken();	// space
					if(token.compareTo(" ") == 0)
						uniformNumber = Integer.parseInt(m_tokenizer.nextToken());	// uniform number
				}
				objInfo = new PlayerInfo(team,uniformNumber);
			}
			else if(token.compareTo("goal") == 0)
			{
				token = m_tokenizer.nextToken();	// space
				if(token.compareTo(" ") == 0)
					token = m_tokenizer.nextToken();	// side
				objInfo = new GoalInfo(token.charAt(0));
			}
			else if(token.compareTo("ball") == 0)
			{
				objInfo = new BallInfo();
			}
			else if(token.compareTo("flag") == 0)
			{
				char type = ' ';
				char posHoriz, posVert;
				token = m_tokenizer.nextToken();	// space
				token = m_tokenizer.nextToken();	// p or [l|c|r]
				if( token.compareTo("p") == 0 )
				{
					type = token.charAt(0);
					token = m_tokenizer.nextToken();	// space
					token = m_tokenizer.nextToken();	// [l|r]
				}
				posHoriz = token.charAt(0);
				token = m_tokenizer.nextToken();	// space
				token = m_tokenizer.nextToken();	// [t|c|b]
				posVert = token.charAt(0);
				objInfo = new FlagInfo(type, posHoriz, posVert);
			}
			else if(token.compareTo("line") == 0)
			{
				token = m_tokenizer.nextToken();	// space
				token = m_tokenizer.nextToken();	// [l|r|t|b]
				objInfo = new LineInfo(token.charAt(0));
			}
			else if(token.compareTo("Player") == 0)
			{
				objInfo = new PlayerInfo();
			}
			else if(token.compareTo("Goal") == 0)
			{
				objInfo = new GoalInfo();
			}
			else if(token.compareTo("Ball") == 0)
			{
				objInfo = new BallInfo();
			}
			else if(token.compareTo("Flag") == 0)
			{
				objInfo = new FlagInfo();
			}
			else if(token.compareTo("Line") == 0)
			{
				objInfo = new LineInfo();
			}

			// Now we should find ")" token
			if(token.compareTo(")") != 0)
				token = m_tokenizer.nextToken();	

			return objInfo;
	}


//===========================================================================
// Public members
	int				m_time;
	Vector		m_objects;


//===========================================================================
// Private members
	private StringTokenizer	m_tokenizer;

}

