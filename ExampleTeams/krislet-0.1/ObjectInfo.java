//
//	File:			ObjectInfo.java
//	Author:		Krzysztof Langner
//	Date:			1997/04/28
//


//***************************************************************************
//
//	This is base class for different classese with visual information
//	about objects
//
//***************************************************************************
class ObjectInfo 
{
//===========================================================================
// Initialization member functions
	public ObjectInfo(String type)
	{
		m_type = type;
	}

	
//===========================================================================
// Public members
	public String		m_type;
	public float		m_distance;
	public float		m_direction;
	public float		m_distChange;
	public float		m_dirChange;
}


//***************************************************************************
//
//	This class holds visual information about player
//
//***************************************************************************
class PlayerInfo extends ObjectInfo
{
//===========================================================================
// Initialization member functions
	public PlayerInfo()
	{
		super("player");
	}

	
	public PlayerInfo(String team, int number)
	{
		super("player");
		m_teamName = team;
		m_uniformName = number;
	}

	
//===========================================================================
// Public members
	String	m_teamName;
	int			m_uniformName;
}


//***************************************************************************
//
//	This class holds visual information about goal
//
//***************************************************************************
class GoalInfo extends ObjectInfo
{
//===========================================================================
// Initialization member functions
	public GoalInfo()
	{
		super("goal");
	}

	public GoalInfo(char side)
	{
		super("goal " + side);
	}

}


//***************************************************************************
//
//	This class holds visual information about ball
//
//***************************************************************************
class BallInfo extends ObjectInfo
{
//===========================================================================
// Initialization member functions
	public BallInfo()
	{
		super("ball");
	}

}


//***************************************************************************
//
//	This class holds visual information about flag
//
//***************************************************************************
class FlagInfo extends ObjectInfo
{
//===========================================================================
// Initialization member functions
	public FlagInfo()
	{
		super("flag");
	}

	
	public FlagInfo(char type, char posHoriz, char posVert)
	{
		super("flag");
		m_type = type;
		m_horiz = posHoriz;
		m_vert = posVert;
	}

	
//===========================================================================
// Public members
	char	m_type;				// 'p'	- penalty area flag
	char	m_horiz;			// l|c|r
	char	m_vert;				// t|c|b
}


//***************************************************************************
//
//	This class holds visual information about line
//
//***************************************************************************
class LineInfo extends ObjectInfo
{
//===========================================================================
// Initialization member functions
	public LineInfo()
	{
		super("line");
	}

	
	public LineInfo(char kind)
	{
		super("line");
		m_kind = kind;
	}

	
//===========================================================================
// Public members
	char	m_kind;				// l|r|t|b
}


