This is sample soccer player written in Java.
It is designed to work with soccer server version 3.xx.

To compile enter
javac *.java

to start
java Krislet 

If you want help on Krislet parameters enter
java Krislet -help

This program was written on Win32 and tested on Linux. 
after starting game this player should move to the ball and kick it
into to goal.

If you have any comments or questions please send them to:
Krzysztof Langner
klang@ydp.com.pl

Have a fun!