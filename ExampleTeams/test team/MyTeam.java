import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;


// to run program: java Main_oneTeam teamName
public class MyTeam {
    public static void main(String[] args) throws Exception {
        String teamName = "noName";
        if (args.length>=0)
          teamName = args[0];
        int nPlayers = 7;
        for (int i = 0; i < (nPlayers-1); ++i) {
          new Thread(player(i, teamName, false)).start();
        }
        new Thread(player(nPlayers-1, teamName, false)).start();
    }

    private static Runnable player(final int num, final String team, Boolean goalie) throws Exception {
        return () -> {
            // num = player number, team = l/r
            int port;
            InetAddress host;

            DatagramSocket clientSocket = null;
            try {
                InetAddress IPAddress = InetAddress.getByName("localhost");
                clientSocket = new DatagramSocket();

                byte[] sendData;
                byte[] receiveData = new byte[1024];
                if (goalie)
                  sendData = ("(init " + team + " (version 15) (goalie))").getBytes(); // connect to server
                else
                  sendData = ("(init " + team + " (version 15))").getBytes(); // connect to server
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 6000);
                clientSocket.send(sendPacket);
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                clientSocket.receive(receivePacket);
                host = receivePacket.getAddress();
                port = receivePacket.getPort();
                String modifiedSentence = new String(receivePacket.getData());
                System.out.println("(init) FROM SERVER (init " + num + "):" + modifiedSentence);

                sendAndReceive(clientSocket, "move", ("10 " + (7 + num)), num, host, port); // init positions (??)
                //sendAndReceive(clientSocket, "move", ("0 "), num, host, port); // init positions (??)

                for (int i = 0; i < 500; ++i) {
                    //System.out.println(i);
                    if(i%20==0) {
                        sendAndReceive(clientSocket, "dash", "250", num, host, port);
                        //sendAndReceive(clientSocket, "dash", "25", num, host, port);
                        Thread.sleep((num+1)*200);
                    } else if(i%11==0) {
                        sendAndReceive(clientSocket, "turn", "37", num, host, port);
                        Thread.sleep((num+1)*200);
                    }
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            } finally {
                if (clientSocket != null) {
                    clientSocket.close();
                }
            }
        };
    }

    public static void sendAndReceive(DatagramSocket clientSocket, String command, String param, int num,
    InetAddress IPAddress, int port) throws Exception {
        // command = i.e. "move", "turn", "dash"
        // param = e.g. "250", "37" ???
        // num = player number
        byte[] sendData;
        byte[] receiveData = new byte[1024];
        sendData = ("(" + command + " " + param + ")"+0).getBytes();
        //sendData = ("(" + command + " " + param + ")"+10).getBytes();
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        clientSocket.receive(receivePacket);
        String modifiedSentence = new String(receivePacket.getData());
        System.out.println("(running) FROM SERVER (" + command + " " + num + "):" + modifiedSentence);
        receivePacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
        clientSocket.send(receivePacket);
    }
}
