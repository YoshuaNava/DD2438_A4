﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;
using TeamZlatan.PlayingUtils;


namespace TeamZlatan
{
    public partial class FieldPlotWindow : Form
    {
        private int cycle;
        private float[,] teamPlayersPositions;
        private float[,] opponentPlayersPositions;
        private float[] ballPosition;
        private float[] ballVelocity;
        private float[] predBallPosition;
        private float[] closestToBallPlayer;
        private float[] ballControllingPlayer;
        private float[] supportingPlayer;
        private float[,] receivingPlayers;
        private List<SupportSpot> supportSpots;
        private List<SupportSpot> tikiTakaSpots;
        float[] blockingInfo;
        float[] goalieBlockingPos;
        float[] goalBounds;
        String currentMode;

        public FieldPlotWindow()
        {
            this.InitializeComponent();
        }

        public void FieldPlotWindow_Load(object sender, EventArgs e)
        {
            PlotModel myModel = PlotRobocupField();
            this.fieldPlot.Model = myModel;
        }


        public PlotModel PlotRobocupField()
        {
            String title = "SoccerField cycle " + cycle + " " + currentMode;
            var myModel = new PlotModel { Title = title };

            if (supportSpots != null)
            {
                var SupportSpotsSeries = new ScatterSeries { MarkerType = MarkerType.Circle, MarkerStroke = OxyColors.Turquoise, MarkerFill = OxyColors.Transparent };
                float x, y, score;
                for (int i = 0; i < supportSpots.Count; i++)
                {
                    x = supportSpots[i].SpotPos[0];
                    y = supportSpots[i].SpotPos[1];
                    score = supportSpots[i].Score;
                    //Console.WriteLine("position: (" + x + ", " + y + "); score = " + score);
                    SupportSpotsSeries.Points.Add(new ScatterPoint(x, y, 2 * score, 0));
                }

                myModel.Series.Add(SupportSpotsSeries);
            }
            if (tikiTakaSpots != null)
            {
                var TikiTakaSpotsSeries = new ScatterSeries { MarkerType = MarkerType.Circle, MarkerStroke = OxyColors.Red, MarkerFill = OxyColors.Transparent };
                float x, y, score;
                for (int i = 0; i < tikiTakaSpots.Count; i++)
                {
                    x = tikiTakaSpots[i].SpotPos[0];
                    y = tikiTakaSpots[i].SpotPos[1];
                    score = tikiTakaSpots[i].Score;
                    //Console.WriteLine("ola (" + x + ", " + y + ") ;  score = " + score);
                    //Console.WriteLine("position: (" + x + ", " + y + "); score = " + score);
                    TikiTakaSpotsSeries.Points.Add(new ScatterPoint(x, y, 4 * score, 0));
                }

                myModel.Series.Add(TikiTakaSpotsSeries);
            }
            if (teamPlayersPositions != null)
            {
                var GoaliePointSeries = new ScatterSeries { MarkerType = MarkerType.Diamond, MarkerStroke = OxyColors.Blue, MarkerFill = OxyColors.Yellow };
                float x = teamPlayersPositions[0, 0];
                float y = teamPlayersPositions[1, 0];
                float theta = teamPlayersPositions[2, 0];
                float xEnd = x + (float)Math.Cos(theta * (Math.PI / 180.0));
                float yEnd = y + (float)Math.Sin(theta * (Math.PI / 180.0));
                GoaliePointSeries.Points.Add(new ScatterPoint(x, y, 8, 200));

                var TeamPointSeries = new ScatterSeries { MarkerType = MarkerType.Square, MarkerStroke = OxyColors.Blue, MarkerFill = OxyColors.Yellow };
                for (int i = 0; i < 11; i++)
                {
                    var TeamOrientationSeries = new LineSeries { Color = OxyColors.Black, MarkerType = MarkerType.Star, MarkerStroke = OxyColors.Black, MarkerFill = OxyColors.Black };
                    x = teamPlayersPositions[0, i];
                    y = teamPlayersPositions[1, i];
                    theta = teamPlayersPositions[2, i];
                    xEnd = x + (float)Math.Cos(theta * (Math.PI / 180.0));
                    yEnd = y + (float)Math.Sin(theta * (Math.PI / 180.0));
                    if (i > 0)
                    {
                        TeamPointSeries.Points.Add(new ScatterPoint(x, y, 6, 0));
                    }
                    TeamOrientationSeries.Points.Add(new DataPoint(x, y));
                    TeamOrientationSeries.Points.Add(new DataPoint(xEnd, yEnd));
                    myModel.Series.Add(TeamOrientationSeries);
                }
                myModel.Series.Add(GoaliePointSeries);
                myModel.Series.Add(TeamPointSeries);
            }
            if (opponentPlayersPositions != null)
            {
                var OpponentPointSeries = new ScatterSeries { MarkerType = MarkerType.Square, MarkerStroke = OxyColors.DarkRed, MarkerFill = OxyColors.DarkRed };
                for (int i = 0; i < 11; i++)
                {
                    float x = opponentPlayersPositions[0, i];
                    float y = opponentPlayersPositions[1, i];
                    OpponentPointSeries.Points.Add(new ScatterPoint(x, y, 6, 0));
                }
                myModel.Series.Add(OpponentPointSeries);
            }

            if (ballPosition != null)
            {
                float x = ballPosition[0];
                float y = ballPosition[1];
                if (ballVelocity != null)
                {
                    var BallVelocitySeries = new LineSeries { MarkerType = MarkerType.Circle, MarkerStroke = OxyColors.Gray, MarkerFill = OxyColors.Gray };
                    float xEnd = ballPosition[0] + ballVelocity[0];
                    float yEnd = ballPosition[1] + ballVelocity[1];
                    BallVelocitySeries.Points.Add(new DataPoint(x, y));
                    BallVelocitySeries.Points.Add(new DataPoint(xEnd, yEnd));
                    myModel.Series.Add(BallVelocitySeries);
                }
                var BallPointSeries = new ScatterSeries { MarkerType = MarkerType.Circle, MarkerStroke = OxyColors.Black, MarkerFill = OxyColors.White };
                BallPointSeries.Points.Add(new ScatterPoint(x, y, 4, 0));
                myModel.Series.Add(BallPointSeries);
            }

            if (goalBounds != null && predBallPosition != null)
            {
                float x = goalBounds[0];
                float y1 = goalBounds[2];
                float y2 = goalBounds[3];
                var IntercectLineSeries = new LineSeries { Color = OxyColors.DarkSeaGreen, MarkerStroke = OxyColors.DarkSeaGreen, MarkerFill = OxyColors.DarkSeaGreen };
                IntercectLineSeries.Points.Add(new DataPoint(x, y1));
                IntercectLineSeries.Points.Add(new DataPoint(predBallPosition[0], predBallPosition[1]));
                IntercectLineSeries.Points.Add(new DataPoint(x, y2));
                myModel.Series.Add(IntercectLineSeries);
            }

            if (goalieBlockingPos != null)
            {
                float x = goalieBlockingPos[0];
                float y = goalieBlockingPos[1];
                var GoalieBlockingPosPointSeries = new ScatterSeries { MarkerType = MarkerType.Diamond, MarkerStroke = OxyColors.Black, MarkerFill = OxyColors.Black };
                GoalieBlockingPosPointSeries.Points.Add(new ScatterPoint(x, y, 4, 0));
                myModel.Series.Add(GoalieBlockingPosPointSeries);
            }

            if (blockingInfo != null && predBallPosition != null)
            {
                float x = blockingInfo[0];
                float y1 = blockingInfo[1];
                float y2 = blockingInfo[2];
                var TestPointSeries = new ScatterSeries { MarkerType = MarkerType.Diamond, MarkerStroke = OxyColors.DarkSeaGreen, MarkerFill = OxyColors.DarkSeaGreen };
                TestPointSeries.Points.Add(new ScatterPoint(x, y1, 4, 0));
                TestPointSeries.Points.Add(new ScatterPoint(x, y2, 4, 0));
                myModel.Series.Add(TestPointSeries);

                if (blockingInfo.Length == 4)
                {
                    float y3 = blockingInfo[3];
                    var IntercectLineSeries = new LineSeries { Color = OxyColors.LightGray, MarkerStroke = OxyColors.LightGray, MarkerFill = OxyColors.LightGray };
                    IntercectLineSeries.Points.Add(new DataPoint(goalBounds[0], y3));
                    IntercectLineSeries.Points.Add(new DataPoint(predBallPosition[0], predBallPosition[1]));
                    myModel.Series.Add(IntercectLineSeries);
                }
            }

            if (predBallPosition != null)
            {
                float x = predBallPosition[0];
                float y = predBallPosition[1];
                var PredBallPointSeries = new ScatterSeries { MarkerType = MarkerType.Circle, MarkerStroke = OxyColors.Black, MarkerFill = OxyColors.Gold };
                PredBallPointSeries.Points.Add(new ScatterPoint(x, y, 4, 0));
                myModel.Series.Add(PredBallPointSeries);
            }

            if (closestToBallPlayer != null)
            {
                var ClosestBallSeries = new ScatterSeries { MarkerType = MarkerType.Circle, MarkerStroke = OxyColors.Black, MarkerFill = OxyColors.Pink };
                float x, y;
                x = closestToBallPlayer[0];
                y = closestToBallPlayer[1];
                ClosestBallSeries.Points.Add(new ScatterPoint(x, y, 4, 0));
                myModel.Series.Add(ClosestBallSeries);
            }

            if (ballControllingPlayer != null)
            {
                var BallControllingPlayerSeries = new ScatterSeries { MarkerType = MarkerType.Circle, MarkerStroke = OxyColors.Black, MarkerFill = OxyColors.SteelBlue };
                float x, y;
                x = ballControllingPlayer[0];
                y = ballControllingPlayer[1];
                BallControllingPlayerSeries.Points.Add(new ScatterPoint(x, y, 4, 0));

                myModel.Series.Add(BallControllingPlayerSeries);
            }

            if (supportingPlayer != null)
            {
                var SupportingPlayerSeries = new ScatterSeries { MarkerType = MarkerType.Circle, MarkerStroke = OxyColors.Black, MarkerFill = OxyColors.Green };
                float x, y;
                x = supportingPlayer[0];
                y = supportingPlayer[1];
                SupportingPlayerSeries.Points.Add(new ScatterPoint(x, y, 4, 0));

                myModel.Series.Add(SupportingPlayerSeries);
            }

            if (receivingPlayers != null)
            {
                var ReceivingPlayersSeries = new ScatterSeries { MarkerType = MarkerType.Circle, MarkerStroke = OxyColors.Black, MarkerFill = OxyColors.Red };
                float x, y;
                for (int i = 0; i < receivingPlayers.GetLength(1); i++)
                {
                    x = receivingPlayers[0, i];
                    y = receivingPlayers[1, i];
                    ReceivingPlayersSeries.Points.Add(new ScatterPoint(x, y, 4, 0));
                }

                myModel.Series.Add(ReceivingPlayersSeries);
            }

            myModel.Axes.Add(new RangeColorAxis() { Position = AxisPosition.Bottom, Minimum = -52.5, Maximum = 52.5, Key = "xAxis" });
            myModel.Axes.Add(new RangeColorAxis() { Position = AxisPosition.Left, Minimum = -34.0, Maximum = 34.0, Key = "yAxis" });
            myModel.PlotAreaBackground = OxyColors.Green;

            return myModel;
        }

        public void UpdatePlayerPositions(int cycle, float[,] newTeamPositions, float[,] newOpponentPlayersPositions = null, float[] ballPosition = null, float[] ballVelocity = null, float[] predBallPosition = null, float[] closestToBallPlayer = null, float[] ballControllingPlayer = null, float[] supportingPlayer = null, float[,] receivingPlayers = null, List<SupportSpot> supportSpots = null, List<SupportSpot> tikiTakaSpots = null)
        {
            this.cycle = cycle;
            this.teamPlayersPositions = newTeamPositions;
            this.opponentPlayersPositions = newOpponentPlayersPositions;
            this.ballPosition = ballPosition;
            this.ballVelocity = ballVelocity;
            this.predBallPosition = predBallPosition;
            this.closestToBallPlayer = closestToBallPlayer;
            this.ballControllingPlayer = ballControllingPlayer;
            this.supportingPlayer = supportingPlayer;
            this.receivingPlayers = receivingPlayers;
            this.supportSpots = supportSpots;
            this.tikiTakaSpots = tikiTakaSpots;

            PlotModel myModel = PlotRobocupField();
            this.fieldPlot.Model = myModel;
            //this.fieldPlot.Refresh();
        }

        public void AddExtraDebug(float[] blockingInfo, float[] goalieBlockingPos, float[] goalBounds, String currentMode)
        {
            this.blockingInfo = blockingInfo;
            this.goalieBlockingPos = goalieBlockingPos;
            this.goalBounds = goalBounds;
            this.currentMode = currentMode;
        }
    }
}
