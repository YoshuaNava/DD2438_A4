﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;
using System.Threading;
using System.Windows.Threading;
using TeamZlatan.Agents;
using TeamZlatan.PlayingUtils;
using TeamZlatan.Util;


/* TODOs:
 * Check why deadlock appears when 2 players from each team both goes to the ball and no one kicks the ball
 * PressingFormation is same as AttackFormation as of now
 * Enabeling the team to run on both sides! Check that nothing is missed in SupportSporCalculator (and other places?)
 * Best way to check if opponents have the ball? Check DoesOpponentsHaveBall in TeamConditions
 * In PlayerActions: GoalieCatchBall - checking if the ball is catched or not could be done better?
 * GoalieMove (in PlayerAction) not done.
 */

namespace TeamZlatan
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.WriteLine("Hej varld");
            String teamName = "TeamZlatan";

            

            Team team = new Team(teamName);

            bool livePlot = true;
            Sounds.PlaySounds = true;

            FieldPlotWindow fieldPlotWindow = new FieldPlotWindow();
            if (livePlot)
            {
                Thread plotWindowThread = new Thread(delegate ()
                {
                    fieldPlotWindow.Show();
                    System.Windows.Threading.Dispatcher.Run();
                });
                plotWindowThread.SetApartmentState(ApartmentState.STA);
                plotWindowThread.Start();
            }

            long prevTime = DateTime.Now.Ticks;


            while (true)
            {
                if (DateTime.Now.Ticks - prevTime >= 100)
                {
                    Console.WriteLine("");
                    Console.WriteLine("#########################");
                    Console.WriteLine("Running team tree update for " + teamName + ", (" + team.TeamSide + ").");
                    team.RunTeamBehaviorTree();
                    Console.WriteLine("Running player tree update.");
                    team.RunPlayersBehaviorTrees();
                    Console.WriteLine("*************************");

                    if (livePlot)
                    {
                        if (fieldPlotWindow.InvokeRequired)
                        {
                            fieldPlotWindow.Invoke((MethodInvoker)delegate ()
                            {
                                fieldPlotWindow.Invalidate();
                                float[,] playerPositions = new float[3, team.TeamPlayers.Count];
                                float[,] receivingPlayers;
                                for (int i = 0; i < team.TeamPlayers.Count; i++)
                                {
                                    playerPositions[0, i] = team.TeamPlayers[i].Pose[0];
                                    playerPositions[1, i] = team.TeamPlayers[i].Pose[1];
                                    playerPositions[2, i] = team.TeamPlayers[i].Pose[2];
                                }

                                fieldPlotWindow.AddExtraDebug(team.blockingInfo, team.GoalieBlockingPosition, team.GameField.GoalBounds, team.CurrentMode);

                                if (team.ReceivingPlayers != null)
                                {
                                    receivingPlayers = new float[2, team.ReceivingPlayers.Count];
                                    for (int i = 0; i < team.ReceivingPlayers.Count; i++)
                                    {
                                        receivingPlayers[0, i] = team.ReceivingPlayers[i].Pose[0];
                                        receivingPlayers[1, i] = team.ReceivingPlayers[i].Pose[1];
                                    }
                                    fieldPlotWindow.UpdatePlayerPositions(team.Cycle, playerPositions, team.OppositeTeamPositions, team.GameBall.Position, team.GameBall.Velocity, team.GameBall.PredPosition, team.ClosestToBallPlayer != null ? team.ClosestToBallPlayer.Pose : null, team.BallControllingPlayer != null ? team.BallControllingPlayer.Pose : null, team.SupportingPlayer != null ? team.SupportingPlayer.Pose : null, receivingPlayers, team.SspotCalculator.SupportSpots, team.TikitakaSpotCalculator.TikitakaSpots);
                                }
                                else
                                {
                                    receivingPlayers = null;
                                    fieldPlotWindow.UpdatePlayerPositions(team.Cycle, playerPositions, team.OppositeTeamPositions, team.GameBall.Position, team.GameBall.Velocity, team.GameBall.PredPosition, team.ClosestToBallPlayer != null ? team.ClosestToBallPlayer.Pose : null, team.BallControllingPlayer != null ? team.BallControllingPlayer.Pose : null, team.SupportingPlayer != null ? team.SupportingPlayer.Pose : null, receivingPlayers, team.SspotCalculator.SupportSpots, null);
                                }


                            });
                        }
                    }
                    prevTime = DateTime.Now.Ticks;
                }
            }


            //teamZlatan.LeaveGame();
#pragma warning disable CS0162 // Se ha detectado código inaccesible
            Console.WriteLine("Hej då " + team.TeamName);
#pragma warning restore CS0162 // Se ha detectado código inaccesible
            Console.ReadKey();
        }
    }
}

