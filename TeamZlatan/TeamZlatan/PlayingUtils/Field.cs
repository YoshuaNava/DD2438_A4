﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamZlatan.PlayingUtils
{
    class Field
    {
        private char teamSide;
        private float[] teamPenaltyAreaBounds = { -52.5F, -36.0F, -20.0F, 20.0F }; // min x, max x, min y, max y
        private float[] goalBounds = { -52.5F, -52.5F, -7.0F, 7.0F }; // min x, max x, min y, max y

        public Field(char teamSide)
        {
            this.teamSide = teamSide;
            if (teamSide == 'r')
            {
                teamPenaltyAreaBounds[0] = -teamPenaltyAreaBounds[0];
                teamPenaltyAreaBounds[1] = -teamPenaltyAreaBounds[1];
                goalBounds[0] = -goalBounds[0];
                goalBounds[1] = -goalBounds[1];
            }
        }

        #region GettersSetters
        public float[] TeamPenaltyAreaBounds { get { return teamPenaltyAreaBounds; } }
        public float[] GoalBounds { get { return goalBounds; } }
        #endregion

        public bool InTeamPenaltyArea(float[] playerPos)
        {
            if (playerPos[1] <= teamPenaltyAreaBounds[3] && playerPos[1] >= teamPenaltyAreaBounds[2])
                if (playerPos[0] <= Math.Max(teamPenaltyAreaBounds[0], teamPenaltyAreaBounds[1]) && playerPos[0] >= Math.Min(teamPenaltyAreaBounds[0], teamPenaltyAreaBounds[1]))
                    return true;
            return false;
        }

        public float[] MidFrontTeamPenaltyArea()
        {
            float[] pos =  { teamPenaltyAreaBounds[1], 0.0F };
            return pos;
        }
    }
}
