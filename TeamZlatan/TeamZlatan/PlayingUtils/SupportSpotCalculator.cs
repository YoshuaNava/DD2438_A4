﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamZlatan.Agents;

namespace TeamZlatan.PlayingUtils
{
    public class SupportSpot
    {
        private float[] spotPos = { 0, 0 };
        private float score;
        private Player playerAssigned;

        public SupportSpot(float[] spotPos)
        {
            this.spotPos = spotPos;
            this.score = 1.0F;
            this.playerAssigned = null;
        }

        #region GettersSetters
        public float[] SpotPos { get { return spotPos; } set { spotPos = value; } }
        public float Score { get { return score; } set { score = value; } }

        internal Player PlayerAssigned { get { return playerAssigned; } set { playerAssigned = value; } }
        #endregion
    }


    class SupportSpotCalculator
    {
        private List<SupportSpot> supportSpots;
        private SupportSpot bestSupportSpot = null;
        private SupportSpot secondSupportSpot = null;
        private int numSpotsX = 6;
        private int numSpotsY = 10;
        private float dx = 8.0F;
        private float dy = 6.0F;
        private float startX = 5.0F;
        private float offsetX = 5.0F;
        private float offsetY = 28.0F;

        private float rewardPassSafe = 2.0F;
        private float rewardCanScore = 2.0F;
        private float rewardEnoughDistance = 1.0F;
        private float rewardCenterPositioning = 2.0F;

        private float spotSafeRadius = 4.0F;
        private float maxDistanceToOppGoal = 25.0F;
        private float maxDistanceToCenter = 5.0F;
        private float maxAngleToOppGoal = 45.0F;
        private float enoughDistancePass = 20.0F;
        private float maxAnglePass = 90.0F;

        private float[] cbMidPoint = { 40.0F, 0.0F };

        public SupportSpotCalculator(char side)
        {
            supportSpots = new List<SupportSpot>();

            if (side == 'r')
            {
                startX -= 50;
                cbMidPoint[0] = -cbMidPoint[0];
            }

            for (int i = 0; i < numSpotsX; i++)
            {
                for (int j = 0; j < numSpotsY; j++)
                {
                    float[] pos = new float[2];
                    pos[0] = startX + dx * i - offsetX;
                    pos[1] = dy * j - offsetY;
                    this.supportSpots.Add(new SupportSpot(pos));
                }
            }
        }

        public SupportSpot DetermineBestSupportSpot(Team team)
        {
            float bestScoreSoFar = 0.0F;

            foreach(SupportSpot spot in this.supportSpots)
            {
                spot.Score = 1.0F;

                if (PassSafeFromOpponents(spot, team))
                {
                    spot.Score += this.rewardPassSafe;
                }

                if (CanShoot(spot, team))
                {
                    spot.Score += this.rewardCanScore;
                }

                //if (EnoughDistanceControllingPlayer(spot, team))
                //{
                //    spot.Score += this.rewardEnoughDistance;
                //}

                if (ProperDistanceFromCenter(spot, team))
                {
                    spot.Score += this.rewardCenterPositioning;
                }

                if (spot.Score >= bestScoreSoFar)
                {
                    bestScoreSoFar = spot.Score;
                }
            }

            supportSpots.Sort((x, y) => y.Score.CompareTo(x.Score));

            //foreach (SupportSpot spot in this.supportSpots)
            //{
            //    Console.WriteLine("Support spot at " + spot.SpotPos[0] + ", " + spot.SpotPos[1] + ")  with score " + spot.Score);
            //}
            //Console.ReadKey();

            //assistSupportSpots = new SupportSpot[2];
            //assistSupportSpots[0] = supportSpots[1];
            //assistSupportSpots[1] = supportSpots[2];
            if (bestSupportSpot == null)
            {
                bestSupportSpot = supportSpots[0];
                secondSupportSpot = supportSpots[1];
            }
            else
            {
                if ((supportSpots[0].SpotPos[0] == supportSpots[1].SpotPos[0]) && (supportSpots[0].SpotPos[1] == supportSpots[1].SpotPos[1]) && (supportSpots[0].Score > supportSpots[1].Score))
                {
                    bestSupportSpot = supportSpots[0];
                    secondSupportSpot = supportSpots[1];
                }
            }

            

            return this.bestSupportSpot;
        }

        private bool PassSafeFromOpponents(SupportSpot spot, Team team)
        {
            if (team.OppositeTeamPositions != null)
            {
                for (int i=0; i<team.OppositeTeamPositions.GetLength(1) ;i++)
                {
                    if(Controllers.EuclideanDistance(team.OppositeTeamPositions[0,i], team.OppositeTeamPositions[1, i], spot.SpotPos[0], spot.SpotPos[1]) <= spotSafeRadius)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private bool CanShoot(SupportSpot spot, Team team)
        {
            
            float[] diffPosition = { (spot.SpotPos[0] - team.OpponentGoal[0]), (spot.SpotPos[1] - team.OpponentGoal[1]) };
            float posDist = Controllers.EuclideanDistance(diffPosition[0], diffPosition[1], 0, 0);

            float angleToGoal = Controllers.Atan2d(diffPosition[1], diffPosition[0]);

            if ((posDist <= maxDistanceToOppGoal) || (Math.Abs(angleToGoal) <= maxAngleToOppGoal))
            {
                return true;
            }

            return false;
        }

        private bool EnoughDistanceControllingPlayer(SupportSpot spot, Team team)
        {
            if (team.BallControllingPlayer != null)
            {
                float[] diffPosition = { (spot.SpotPos[0] - team.BallControllingPlayer.Pose[0]), (spot.SpotPos[1] - team.BallControllingPlayer.Pose[1]) };
                float posDist = Controllers.EuclideanDistance(diffPosition[0], diffPosition[1], 0, 0);

                float anglePass = Controllers.Atan2d(diffPosition[1], diffPosition[0]);

                if ((posDist >= enoughDistancePass) && (Math.Abs(anglePass) <= maxAnglePass))
                {
                    return true;
                }
            }

            return false;
        }

        private bool ProperDistanceFromCenter(SupportSpot spot, Team team)
        {
            if (team.BallControllingPlayer != null)
            {
                float[] diffPosition = { (spot.SpotPos[0] - cbMidPoint[0]), (spot.SpotPos[1] - cbMidPoint[1]) };
                float posDist = Controllers.EuclideanDistance(diffPosition[0], diffPosition[1], 0, 0);

                if (posDist <= maxDistanceToCenter)
                {
                    return true;
                }
            }

            return false;
        }

        #region GettersSetters
        public List<SupportSpot> SupportSpots { get { return supportSpots; } set { supportSpots = value; } }
        public SupportSpot BestSupportSpot { get { return bestSupportSpot; } set { bestSupportSpot = value; } }
        public SupportSpot SecondSupportSpot { get { return secondSupportSpot; } set { secondSupportSpot = value; } }
        #endregion

    }
}
