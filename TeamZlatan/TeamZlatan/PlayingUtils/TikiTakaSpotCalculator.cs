﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamZlatan.Agents;

namespace TeamZlatan.PlayingUtils
{



    class TikiTakaSpotCalculator
    {
        private List<SupportSpot> tikitakaSpots;
        private SupportSpot bestTikitakaSpot = null;

        private float rewardPassSafe = 3.0F;
        private float rewardPassForward = 2.0F;
        private float rewardEnoughDistance = 1.0F;
#pragma warning disable CS0414 // El campo 'TikiTakaSpotCalculator.rewardCenterPositioning' está asignado pero su valor nunca se usa
        private float rewardCenterPositioning = 2.0F;
#pragma warning restore CS0414 // El campo 'TikiTakaSpotCalculator.rewardCenterPositioning' está asignado pero su valor nunca se usa

        private float spotSafeRadius = 2.0F;
        private float maxDistanceToCenter = 5.0F;
        private float enoughDistancePass = 20.0F;
        private float maxAnglePass = 90.0F;
        //private float maxDistanceToOppGoal = 25.0F;
        //private float maxAngleToOppGoal = 45.0F;

        private float[] cbMidPoint = { 35.0F, 0.0F };

        public TikiTakaSpotCalculator(List<Player> receivingPlayers)
        {
            tikitakaSpots = new List<SupportSpot>();

            if (receivingPlayers != null)
            {
                for (int i = 0; i < receivingPlayers.Count; i++)
                {
                    float[] pos = { receivingPlayers[i].Pose[0], receivingPlayers[i].Pose[1] };
                    this.tikitakaSpots.Add(new SupportSpot(pos));
                }

                char teamSide = receivingPlayers[0].TeamSide;
                if(teamSide == 'l')
                {
                    cbMidPoint = new float[2] { 35.0F, 0.0F };
                }
                else
                {
                    cbMidPoint = new float[2] { -35.0F, 0.0F };
                }
            }
        }

        public SupportSpot DetermineBestTikiTakaSpot(Team team)
        {
            this.bestTikitakaSpot = null;
            float bestScoreSoFar = 0.0F;

            foreach(SupportSpot spot in this.tikitakaSpots)
            {
                spot.Score = 1.0F;

                if (PassSafeFromOpponents(spot, team))
                {
                    spot.Score += this.rewardPassSafe;
                }

                if (PassForward(spot, team))
                {
                    spot.Score += this.rewardPassForward;
                }

                if (EnoughDistanceControllingPlayer(spot, team))
                {
                    spot.Score += this.rewardEnoughDistance;
                }

                //if (ProperDistanceFromCenter(spot, team))
                //{
                //    spot.Score += this.rewardCenterPositioning;
                //}

                if (spot.Score >= bestScoreSoFar)
                {
                    bestScoreSoFar = spot.Score;
                    bestTikitakaSpot = spot;
                }
            }

            return this.bestTikitakaSpot;
        }



        private bool PassSafeFromOpponents(SupportSpot spot, Team team)
        {
            if (team.OppositeTeamPositions != null)
            {
                for (int i=0; i<team.OppositeTeamPositions.GetLength(1) ;i++)
                {
                    if(Controllers.EuclideanDistance(team.OppositeTeamPositions[0,i], team.OppositeTeamPositions[1, i], spot.SpotPos[0], spot.SpotPos[1]) <= spotSafeRadius)
                    {
                        return false;
                    }
                }
            }

            return true;
        }


        private bool PassForward(SupportSpot spot, Team team)
        {
            if(((team.GameBall.Position[0] < spot.SpotPos[0]) && (team.TeamSide == 'l')) || ((team.GameBall.Position[0] > spot.SpotPos[0]) && (team.TeamSide == 'r')))
            {
                return true;
            }

            return false;
        }

        private bool EnoughDistanceControllingPlayer(SupportSpot spot, Team team)
        {
            if (team.BallControllingPlayer != null)
            {
                float[] diffPosition = { (spot.SpotPos[0] - team.BallControllingPlayer.Pose[0]), (spot.SpotPos[1] - team.BallControllingPlayer.Pose[1]) };
                float posDist = Controllers.EuclideanDistance(diffPosition[0], diffPosition[1], 0, 0);

                float anglePass = Controllers.Atan2d(diffPosition[1], diffPosition[0]);

                if ((posDist >= enoughDistancePass) && (Math.Abs(anglePass) <= maxAnglePass))
                {
                    return true;
                }
            }

            return false;
        }


        private bool ProperDistanceFromCenter(SupportSpot spot, Team team)
        {
            if (team.BallControllingPlayer != null)
            {
                float[] diffPosition = { (spot.SpotPos[0] - cbMidPoint[0]), (spot.SpotPos[1] - cbMidPoint[1]) };
                float posDist = Controllers.EuclideanDistance(diffPosition[0], diffPosition[1], 0, 0);

                if (posDist <= maxDistanceToCenter)
                {
                    return true;
                }
            }

            return false;
        }

        #region GettersSetters
        public List<SupportSpot> TikitakaSpots { get { return tikitakaSpots; } set { tikitakaSpots = value; } }
        public SupportSpot BestTikitakaSpot { get { return bestTikitakaSpot; } set { bestTikitakaSpot = value; } }
        #endregion

    }
}
