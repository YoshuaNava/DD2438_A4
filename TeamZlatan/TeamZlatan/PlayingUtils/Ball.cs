﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamZlatan.Agents;

namespace TeamZlatan.PlayingUtils
{
    class Ball
    {
        private float[] position = { 0.0F, 0.0F };
        private float[] velocity = { 0.0F, 0.0F };

        private float[] predPosition = null;

        private const float epsilon = 0.1F;
        private const float ballDecay = 0.94F;
        private const float kickPowerRate = 0.027F;

        
        public Ball()
        { }

        #region GettersSetters
        public float[] Position { get { return position; } set { position = value; } }
        public float[] Velocity { get { return velocity; } set { velocity = value; } }
        public float[] PredPosition { get { return predPosition; } set { predPosition = value; } }
        #endregion


        public float[] PredictStoppingPosition()
        {
            float[] accel = { 0, 0 };
            int itr = 150;
            float totalDecay = (float)(Math.Pow(ballDecay, itr + 1) - 1) / (ballDecay - 1);

            predPosition = new float[2] { position[0] + (velocity[0] + accel[0]) * totalDecay, position[1] + (velocity[1] + accel[1]) * totalDecay };

            //Console.WriteLine("                         InitialPos (" + position[0] + ", " + position[1] + ")");
            //Console.WriteLine("                         GoalPos (" + predPosition[0] + ", " + predPosition[1] + ")");
            //Console.WriteLine("                         totalDecay " + totalDecay);
            //Console.WriteLine("                         velocity (" + velocity[0] + ", " + velocity[1] + ")");
            //Console.ReadKey();

            return predPosition;
        }


        /* Function to calculate the final position of the ball after being kicked */
        public float[] PredictStoppingPosition(float kickPower, float kickAngle)
        {
            float[] accel = { kickPower * kickPowerRate * Controllers.Cosd(kickAngle), kickPower * kickPowerRate * Controllers.Sind(kickAngle) };
            int itr = 150;            
            float totalDecay = (float) (Math.Pow(ballDecay, itr+1) - 1) / (ballDecay - 1);

            predPosition = new float[2] { position[0] + (velocity[0] + accel[0]) * totalDecay, position[1] + (velocity[1] + accel[1]) * totalDecay };

            return predPosition;
        }

        /* Function to predict the amount of power needed to make the ball reach a given goal position */
        public float PredictPowerToReachPos(float[] goalPos, float kickAngle)
        {
            float power = 0;
            float[] posDiff = { goalPos[0] - position[0], goalPos[1] - position[1] };
            float[] ballDir = { Controllers.Cosd(kickAngle), Controllers.Sind(kickAngle) };
            float totalDecay = (float)(Math.Pow(ballDecay, 150 + 1) - 1) / (ballDecay - 1);
            power = Controllers.EuclideanNorm(posDiff[0], posDiff[1]) / (Controllers.EuclideanNorm(ballDir[0], ballDir[1]) * totalDecay * kickPowerRate);

            //Console.WriteLine("                         InitialPos (" + position[0] + ", " + position[1] + ")");
            //Console.WriteLine("                         GoalPos (" + goalPos[0] + ", " + goalPos[1] + ")");
            //Console.WriteLine("                         totalDecay " + totalDecay);
            //Console.WriteLine("                         kickPowerRate " + kickPowerRate);
            //Console.WriteLine("                         power " + power);
            //Console.ReadKey();

            return power;
        }

        /* Function that determines is a pass is under the risk of being intercepted, given a certain threshold on its distance from the opponents */
        public bool RiskOfInterception(float[] goalPos, float[] opponentPosition, float opponentDistThresh)
        {
            float dx = goalPos[0] - position[0];
            float dy = goalPos[1] - position[1];

            if(dx != 0)
            {
                /* Point-line distance, as presented in: https://brilliant.org/wiki/dot-product-distance-between-point-and-a-line/ */
                float denom = (float) Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2));
                float c = - dy * position[0] - dx * position[1];
                float numer = (float) Math.Abs(dy * opponentPosition[0] + dx * opponentPosition[1] + c);
                if(numer/denom <= opponentDistThresh)
                {
                    return true;
                }
            }
            else
            {
                if(Math.Abs(goalPos[0] - opponentPosition[0]) <= opponentDistThresh)
                {
                    return true;
                }
            }

            return false;
        }

        

        public char GetSide()
        {
            if (position[0] < 0)
                return 'l';
            return 'r';
        }
    }
}
