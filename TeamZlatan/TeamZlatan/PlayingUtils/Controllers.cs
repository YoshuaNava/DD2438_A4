﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TeamZlatan.PlayingUtils
{
    public class Controllers
    {
        public const float dt = 0.100F;
        public const float Kp_arrive = 1.0F;

        public const float formationErrorTolerance = 1.0F;

        /* Distance inside which a player is considered as controlling the ball */
        public const float ballControlDistance = 3.5F;
        public const float ballControlVel = 0.3F;

        /* Distance thresholds for measuring how probable is an interception after a shoot or pass */
        public const float opponentsClosenessThresh = 6.0F;
        public const float passInterceptionThresh = 2.0F;
        public const float shootInterceptionThresh = 1.0F;

        public const float AngleDiffThresh = 15.0F;
        public const float DistThresh = 0.5F;
        public const float facingDistThresh = 3.0F;
        public const float facingAngleThresh = 30.0F;

        public static float NormalizeAngleDegrees(float angle)
        {
            double angleRadians = angle * (Math.PI / 180.0);
            return (float) (Math.Atan2(Math.Sin(angleRadians), Math.Cos(angleRadians)) * (180.0 / Math.PI));
        }


        public static float Cosd(float angleDegrees)
        {
            return (float) Math.Cos(angleDegrees * (Math.PI / 180.0));
        }

        public static float Sind(float angleDegrees)
        {
            return (float) Math.Sin(angleDegrees * (Math.PI / 180.0));
        }

        public static float Tand(float angleDegrees)
        {
            return (float) Math.Tan(angleDegrees * (Math.PI / 180.0));
        }

        public static float Atan2d(float y, float x)
        {
            return (float) (Math.Atan2(y, x) * (180.0 / Math.PI));
        }

        public static float EuclideanDistance(float x1, float y1, float x2, float y2)
        {
            return (float) (Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2)));
        }

        public static float EuclideanNorm(float x1, float y1)
        {
            return (float)(Math.Sqrt(Math.Pow(x1, 2) + Math.Pow(y1, 2)));
        }

    }
}
