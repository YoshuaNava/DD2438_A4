﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace TeamZlatan.Util
{
    class RCSSSConnection
    {
        #region Attributes
        private String serverIp = "127.0.0.1";
        private int serverPort;

        private UdpClient serverSocket = null;
        private IPEndPoint ipEndpoint = null;

        private bool log = false; // if you want to save the client & server messages in a text file
        private String logFileName;
        #endregion 

        public RCSSSConnection(int port)
        {
            this.serverPort = port;
            this.serverSocket = new UdpClient();
            this.ipEndpoint = new IPEndPoint(IPAddress.Parse(this.serverIp), this.serverPort);

            if (log)
            {
                var timestamp = (int)(DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds;
                logFileName = "Log_" + timestamp.ToString();
            }

        }

        public String Send(String message, bool verbose = false)
        {
            if (this.serverSocket != null)
            {
                byte[] byteMsg = System.Text.Encoding.UTF8.GetBytes(message);

                var sentData = serverSocket.Send(byteMsg, message.Length, this.ipEndpoint);

                try
                {
                    byte[] bytesReceived = serverSocket.Receive(ref this.ipEndpoint);
                    string reply = Encoding.ASCII.GetString(bytesReceived);
                    if (verbose)
                    {
                        Console.WriteLine("Reply:" + reply);
                    }
                    if (log)
                        LogMessage("../../../../Log/" + logFileName + ".txt", reply, message);
                    return reply;
                }
#pragma warning disable CS0168 // La variable 'e' se ha declarado pero nunca se usa
                catch (Exception e)
#pragma warning restore CS0168 // La variable 'e' se ha declarado pero nunca se usa
                {
                    //throw e;
                    return null;
                }
            }
            else
            {
                Console.WriteLine("Error. Server socket not initialized.");
                return null;
            }
        }

        public String Listen()
        {
            String message = null;
            if (this.serverSocket != null)
            {
                try
                {
                    byte[] bytesReceived = serverSocket.Receive(ref this.ipEndpoint);
                    message = Encoding.ASCII.GetString(bytesReceived);
                }
#pragma warning disable CS0168 // La variable 'e' se ha declarado pero nunca se usa
                catch (Exception e)
#pragma warning restore CS0168 // La variable 'e' se ha declarado pero nunca se usa
                {
                    message = null;
                }
            }
            if(log)
                LogMessage("../../../../Log/" + logFileName + ".txt", message);
            return message;
        }

        public void LogMessage(String filePath, String serverReply, String clientMessage = null)
        {

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@filePath, true))
            {
                file.WriteLine("-----");
                if (clientMessage != null)
                {
                    file.WriteLine("Client:");
                    file.WriteLine(clientMessage);
                }
                if (serverReply != null)
                {
                    file.WriteLine("Server:");
                    file.WriteLine(serverReply);
                }
            }
        }

        public bool DataAvailable()
        {
            return (serverSocket.Available > 0);
        }
    }
}
