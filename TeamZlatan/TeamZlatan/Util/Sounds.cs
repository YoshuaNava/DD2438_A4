﻿using System;
using System.Collections.Generic;
using System.Media;

namespace TeamZlatan.Util
{
    public class Sounds
    {
        //private static String soundFolderPath = "../../Sounds/";
        private static String soundFolderPath = "../../../../Sounds/";
        private static SoundPlayer stadiumNoisePlayer = new SoundPlayer();
        private static bool playSounds = true;

        public static bool PlaySounds { get { return playSounds; } set { playSounds = value; } }

        public static void PlayStadiumNoise()
        {
            if (playSounds)
            {
                stadiumNoisePlayer.SoundLocation = soundFolderPath + "Large_Stadium" + ".wav";
                stadiumNoisePlayer.PlayLooping();
            }
        }

        public static void Passing()
        {
            if (playSounds)
            {
                SoundPlayer player = new SoundPlayer();
                Random rnd = new Random();
                int i = rnd.Next(1, 4);
                if (i == 1)
                    player.SoundLocation = soundFolderPath + "03magesterial" + ".wav";
                else if (i == 2)
                    player.SoundLocation = soundFolderPath + "02jawdroppinggenius" + ".wav";
                else
                    player.SoundLocation = soundFolderPath + "09absolutelyastonishing" + ".wav";

                player.Play();
            }
        }

        public static void GoalScream()
        {
            if (playSounds)
            {
                SoundPlayer player = new SoundPlayer();
                Random rnd = new Random();
                int i = rnd.Next(1, 5);
                if (i == 1)
                    player.SoundLocation = soundFolderPath + "36scream6" + ".wav";
                else if (i == 2)
                    player.SoundLocation = soundFolderPath + "04magnifico" + ".wav";
                else if (i == 3)
                    player.SoundLocation = soundFolderPath + "10bravadoconcerto" + ".wav";
                else if (i == 4)
                    player.SoundLocation = soundFolderPath + "27magic" + ".wav";
                else
                    player.SoundLocation = soundFolderPath + "21puregenius" + ".wav";
                player.Play();
            }
        }

        public static void Brave()
        {
            if (playSounds)
            {
                SoundPlayer player = new SoundPlayer();
                player.SoundLocation = soundFolderPath + "01matadorinhighheels" + ".wav";
                player.Play();
            }
        }

        public static void LookOut()
        {
            if (playSounds)
            {
                SoundPlayer player = new SoundPlayer();
                Random rnd = new Random();
                int i = rnd.Next(1, 3);
                if (i == 1)
                    player.SoundLocation = soundFolderPath + "07lookout" + ".wav";
                else
                    player.SoundLocation = soundFolderPath + "35scream5" + ".wav";
                player.Play();
            }
        }
    }
       
}
