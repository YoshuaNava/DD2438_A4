﻿namespace TeamZlatan
{
    partial class FieldPlotWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fieldPlot = new OxyPlot.WindowsForms.PlotView();
            this.SuspendLayout();
            // 
            // fieldPlot
            // 
            this.fieldPlot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fieldPlot.Location = new System.Drawing.Point(0, 0);
            this.fieldPlot.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.fieldPlot.Name = "fieldPlot";
            this.fieldPlot.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.fieldPlot.Size = new System.Drawing.Size(645, 384);
            this.fieldPlot.TabIndex = 0;
            this.fieldPlot.Text = "Hola";
            this.fieldPlot.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.fieldPlot.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.fieldPlot.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // FieldPlotWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 1024);
            this.Controls.Add(this.fieldPlot);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FieldPlotWindow";
            this.Text = "FieldPlot (WindowsForms)";
            this.ResumeLayout(false);

        }

        #endregion

        private OxyPlot.WindowsForms.PlotView fieldPlot;
    }
}