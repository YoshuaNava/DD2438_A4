﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using TeamZlatan.BehaviorTrees;
using TeamZlatan.PlayingUtils;
using TeamZlatan.Util;

namespace TeamZlatan.Agents
{
    class IsInitFormationSet : ConditionNode
    {
        public IsInitFormationSet()
        {
            this.name = "IsInitFormationSet";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            float[] pose = this.data["player"].Pose;
            float[] positionInFormation = this.data["player"].PositionInFormation;
            double dist = Math.Sqrt(Math.Pow(pose[0] - positionInFormation[0], 2) + Math.Pow(pose[1] - positionInFormation[1], 2));

            if (dist < Controllers.formationErrorTolerance)
            {
                data["player"].FormationSet = true;
            }
            else
            {
                data["player"].FormationSet = false;
            }

            return NodeStatus.SUCCESS;
        }
    }

    class IsPlayerClosestToBall : ConditionNode
    {
        public IsPlayerClosestToBall()
        {
            this.name = "IsPlayerClosestToBall";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            if (data["player"] == data["team"].ClosestToBallPlayer)
            {
                if(verbose)
                    Console.WriteLine("                     YES!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

                data["player"].GoalPosition[0] = data["team"].GameBall.PredPosition[0];
                data["player"].GoalPosition[1] = data["team"].GameBall.PredPosition[1];
                return NodeStatus.SUCCESS;
            }
            else
            {
                return NodeStatus.FAILURE;
            }
        }
    }

    class IsBallControllingPlayer : ConditionNode
    {
        public IsBallControllingPlayer()
        {
            this.name = "IsBallControllingPlayer";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            if (data["player"] == data["team"].BallControllingPlayer)
            {
                if (verbose)
                    Console.WriteLine("                     YES!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                data["player"].GoalPosition[0] = data["team"].GameBall.Position[0];
                data["player"].GoalPosition[1] = data["team"].GameBall.Position[1];
                return NodeStatus.SUCCESS;
            }
            else
            {
                return NodeStatus.FAILURE;
            }
        }
    }

    class IsSupportingPlayer : ConditionNode
    {
        public IsSupportingPlayer()
        {
            this.name = "IsSupportingPlayer";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            if (data["player"] == data["team"].SupportingPlayer)
            {
                if (verbose)
                    Console.WriteLine("                     YES!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

                data["player"].GoalPosition[0] = data["team"].SspotCalculator.SecondSupportSpot.SpotPos[0];
                data["player"].GoalPosition[1] = data["team"].SspotCalculator.SecondSupportSpot.SpotPos[1];

                //if (data["player"].IsOffside == true)
                //{
                //    float deepestDefenderX = 0;
                //    if (data["team"].BallControllingPlayer != null)
                //        if (data["team"].TeamSide == 'l')
                //        {
                //            if ((data["team"].BallControllingPlayer.Pose[0] > 0) && (data["team"].BallControllingPlayer.Pose[0] > deepestDefenderX))
                //            {
                //                deepestDefenderX = data["team"].BallControllingPlayer.Pose[0];
                //            }
                //        }
                //        else
                //        {
                //            if ((data["team"].BallControllingPlayer.Pose[0] < 0) && (data["team"].BallControllingPlayer.Pose[0] < deepestDefenderX))
                //            {
                //                deepestDefenderX = data["team"].BallControllingPlayer.Pose[0];
                //            }
                //        }

                //    if (((data["team"].SupportingPlayer.Pose[0] > 0.0) && (data["team"].TeamSide == 'l')) || ((data["team"].SupportingPlayer.Pose[0] < 0.0) && (data["team"].TeamSide == 'r')))
                //    {
                //        data["team"].SupportingPlayer.GoalPosition[0] = deepestDefenderX;
                //        Console.WriteLine("OFFSIDE !!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                //    }
                //}
                //else
                //{
                //    if (data["team"].TeamSide == 'l')
                //    {
                //        float deepestDefenderX = 0;
                //        float[] pose = data["player"].Pose;
                //        for (int i = 0; i < data["team"].OppositeTeamPositions.GetLength(1); i++)
                //        {
                //            if (data["team"].OppositeTeamPositions[0, i] > deepestDefenderX)
                //            {
                //                deepestDefenderX = data["team"].OppositeTeamPositions[0, i];
                //            }
                //        }

                //        if (data["team"].BallControllingPlayer != null)
                //            if ((data["team"].BallControllingPlayer.Pose[0] > 0) && (data["team"].BallControllingPlayer.Pose[0] > deepestDefenderX))
                //            {
                //                deepestDefenderX = data["team"].BallControllingPlayer.Pose[0];
                //            }

                //        if (data["team"].SupportingPlayer.Pose[0] > deepestDefenderX)
                //        {
                //            data["team"].SupportingPlayer.GoalPosition[0] = deepestDefenderX;
                //        }
                //    }
                //    else
                //    {
                //        float deepestDefenderX = 0;
                //        float[] pose = data["player"].Pose;
                //        for (int i = 0; i < data["team"].OppositeTeamPositions.GetLength(1); i++)
                //        {
                //            if (data["team"].OppositeTeamPositions[0, i] < deepestDefenderX)
                //            {
                //                deepestDefenderX = data["team"].OppositeTeamPositions[0, i];
                //            }
                //        }

                //        if (data["team"].BallControllingPlayer != null)
                //            if ((data["team"].BallControllingPlayer.Pose[0] < 0) && (data["team"].BallControllingPlayer.Pose[0] < deepestDefenderX))
                //            {
                //                deepestDefenderX = data["team"].BallControllingPlayer.Pose[0];
                //            }

                //        if (data["team"].SupportingPlayer.Pose[0] < deepestDefenderX)
                //        {
                //            data["team"].SupportingPlayer.GoalPosition[0] = deepestDefenderX;
                //        }
                //    }
                //}
                return NodeStatus.SUCCESS;
            }
            else
            {
                return NodeStatus.FAILURE;
            }
        }
    }

    class IsReceivingPlayer : ConditionNode
    {
        public IsReceivingPlayer()
        {
            this.name = "IsReceivingPlayer";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            if (data["team"].ReceivingPlayers != null)
            {
                foreach (Player p in data["team"].ReceivingPlayers)
                {
                    if (data["player"] == p)
                    {
                        if (verbose)
                            Console.WriteLine("                     YES!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        return NodeStatus.SUCCESS;
                    }
                }
            }
            if (verbose)
                Console.WriteLine("                     NO");
            return NodeStatus.FAILURE;
        }
    }

    class IsThroughPassPossible : ConditionNode
    {
        public IsThroughPassPossible()
        {
            this.name = "IsThroughPassPossible";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            if (data["team"].SupportingPlayer != null)
            {
                if (data["team"].FeasiblePass(data["team"].SupportingPlayer))
                {
                    if (verbose)
                        Console.WriteLine("                     YES!!!!!!!!!!!!!!!!");

                    return NodeStatus.SUCCESS;
                }
            }

            return NodeStatus.FAILURE;
        }
    }

    class IsOpponentNotVeryClose : ConditionNode
    {
        public IsOpponentNotVeryClose()
        {
            this.name = "IsOpponentNotVeryClose";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            /* if there is another team */
            if (data["team"].OppositeTeamPositions != null)
            {
                for (int i = 0; i < data["team"].OppositeTeamPositions.GetLength(1); i++)
                {
                    if (Controllers.EuclideanDistance(data["team"].OppositeTeamPositions[0,i], data["team"].OppositeTeamPositions[1, i], data["player"].Pose[0], data["player"].Pose[1]) <= Controllers.opponentsClosenessThresh)
                    {
                        float[] diffPosition = { (data["team"].OppositeTeamPositions[0, i] - data["team"].GameBall.Position[0]), (data["team"].OppositeTeamPositions[1, i] - data["team"].GameBall.Position[1]) };

                        float angleOfBall = Controllers.Atan2d(diffPosition[1], diffPosition[0]);
                        float angleDiff = angleOfBall - data["team"].OppositeTeamPositions[2, i];
                        if(Math.Abs(angleDiff) <= Controllers.AngleDiffThresh)
                        {
                            return NodeStatus.FAILURE;
                        }
                    }
                }
            }

            if (verbose)
                Console.WriteLine("                     YES!!!!!!!!!!!!!!!!");

            data["player"].GoalPosition[0] = data["team"].GameBall.Position[0];
            data["player"].GoalPosition[1] = data["team"].GameBall.Position[1];

            return NodeStatus.SUCCESS;
        }
    }


    class IsBallKickableTowardsGoal : ConditionNode
    {
        public IsBallKickableTowardsGoal()
        {
            this.name = "IsBallKickableTowardsGoal";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            float[] diffPosBall = { (data["team"].GameBall.Position[0] - data["player"].Pose[0]), (data["team"].GameBall.Position[1] - data["player"].Pose[1]) };
            float ballDist = Controllers.EuclideanNorm(diffPosBall[0], diffPosBall[1]);

            if (ballDist <= Controllers.DistThresh)
            {
                if (verbose)
                    Console.WriteLine("                     YES!!!!!!!!!!!!!!!!");

                Console.WriteLine("                     GOAL POSITION");
                //data["player"].GoalPosition[0] = data["team"].OpponentGoal[0];
                //data["player"].GoalPosition[1] = data["team"].OpponentGoal[1];
                data["player"].GoalPosition[0] = data["team"].SspotCalculator.BestSupportSpot.SpotPos[0];
                data["player"].GoalPosition[1] = data["team"].SspotCalculator.BestSupportSpot.SpotPos[1];
            }

            return NodeStatus.SUCCESS;
        }
    }


    class IsOtherPlayerCloseAndFacing : ConditionNode
    {
        public IsOtherPlayerCloseAndFacing()
        {
            this.name = "IsOtherPlayerCloseAndFacing";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            /* if there is another other team */
            if (data["team"].OppositeTeamPositions != null)
            {
                for (int i = 0; i < data["team"].OppositeTeamPositions.GetLength(1); i++)
                {
                    float[] diffPositionPlayers = { (data["team"].OppositeTeamPositions[0, i] - data["player"].Pose[0]), (data["team"].OppositeTeamPositions[1, i] - data["player"].Pose[1]) };
                    float[] diffPositionGoalOtherPlayer = { (data["team"].OppositeTeamPositions[0, i] - data["player"].GoalPosition[0]), (data["team"].OppositeTeamPositions[1, i] - data["player"].GoalPosition[1]) };
                    float[] diffPositionGoalMe = { (data["player"].Pose[0] - data["player"].GoalPosition[0]), (data["player"].Pose[1] - data["player"].GoalPosition[1]) };

                    if (Controllers.EuclideanNorm(diffPositionPlayers[0], diffPositionPlayers[1]) <= Controllers.facingDistThresh)
                    {
                        if (Controllers.EuclideanNorm(diffPositionGoalOtherPlayer[0], diffPositionGoalOtherPlayer[1]) < Controllers.EuclideanNorm(diffPositionGoalMe[0], diffPositionGoalMe[0]))
                        {
                            float goalYup = (float)(Math.Sqrt(Math.Pow(Controllers.facingDistThresh, 2) - Math.Pow(diffPositionPlayers[0], 2))) + data["team"].OppositeTeamPositions[1, i];
                            float goalYdown = (float)(-Math.Sqrt(Math.Pow(Controllers.facingDistThresh, 2) - Math.Pow(diffPositionPlayers[0], 2))) + data["team"].OppositeTeamPositions[1, i];

                            data["player"].GoalPosition[0] = data["team"].OppositeTeamPositions[0, i];
                            if (Math.Abs(goalYup - data["player"].Pose[1]) >= Math.Abs(goalYdown - data["player"].Pose[1]))
                            {
                                data["player"].GoalPosition[1] = goalYdown;
                            }
                            else //if (Math.Abs(goalYup - data["player"].Pose[1]) < Math.Abs(goalYdown - data["player"].Pose[1]))
                            {
                                data["player"].GoalPosition[1] = goalYup;
                            }

                            if (verbose)
                                Console.WriteLine("                     YES!!!!!!!!!!!!!!!!");

                            return NodeStatus.SUCCESS;
                        }
                    }
                }
            }

            foreach(Player p in data["team"].TeamPlayers)
            {
                if(p != data["player"])
                {
                    float[] diffPositionPlayers = { (p.Pose[0] - data["player"].Pose[0]), (p.Pose[1] - data["player"].Pose[1]) };
                    float[] diffPositionGoalOtherPlayer = { (p.Pose[0] - data["player"].GoalPosition[0]), (p.Pose[1] - data["player"].GoalPosition[1]) };
                    float[] diffPositionGoalMe = { (data["player"].Pose[0] - data["player"].GoalPosition[0]), (data["player"].Pose[1] - data["player"].GoalPosition[1]) };

                    if (Controllers.EuclideanNorm(diffPositionPlayers[0], diffPositionPlayers[1]) <= Controllers.facingDistThresh)
                    {
                        if (Controllers.EuclideanNorm(diffPositionGoalOtherPlayer[0], diffPositionGoalOtherPlayer[1]) < Controllers.EuclideanNorm(diffPositionGoalMe[0], diffPositionGoalMe[0]))
                        {

                            float goalYup = (float)(Math.Sqrt(Math.Pow(Controllers.facingDistThresh, 2) - Math.Pow(diffPositionPlayers[0], 2))) + p.Pose[1];
                            float goalYdown = (float)(-Math.Sqrt(Math.Pow(Controllers.facingDistThresh, 2) - Math.Pow(diffPositionPlayers[0], 2))) + p.Pose[1];

                            data["player"].GoalPosition[0] = p.Pose[0];
                            if (Math.Abs(goalYup - data["player"].Pose[1]) >= Math.Abs(goalYdown - data["player"].Pose[1]))
                            {
                                data["player"].GoalPosition[1] = goalYdown;
                            }
                            else //if (Math.Abs(goalYup - data["player"].Pose[1]) < Math.Abs(goalYdown - data["player"].Pose[1]))
                            {

                                data["player"].GoalPosition[1] = goalYup;
                            }

                            if (verbose)
                                Console.WriteLine("                     YES!!!!!!!!!!!!!!!!");

                            return NodeStatus.SUCCESS;
                        }
                        
                    }
                }
            }

            return NodeStatus.FAILURE;
        }
    }


    class AreReceiversAround : ConditionNode
    {
        public AreReceiversAround()
        {
            this.name = "AreReceiversAround";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            if (data["team"].ReceivingPlayers != null)
            {
                if (data["team"].ReceivingPlayers.Count > 0)
                {
                    if (verbose)
                        Console.WriteLine("                     YES!!!!!!!!!!!!!!!!");

                    return NodeStatus.SUCCESS;
                }
            }

            return NodeStatus.FAILURE;
        }
    }

    class IsScoringPossible : ActionNode
    {
        public IsScoringPossible()
        {
            this.name = "IsScoringPossible";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            if (data["player"] == data["team"].BallControllingPlayer)
            {
                float[] ballControllingPlayerPos = this.data["player"].Pose;
                float[] goalPos = this.data["team"].OpponentGoal;

                float[] diffPosition = { (goalPos[0] - ballControllingPlayerPos[0]), (goalPos[1] - ballControllingPlayerPos[1]) };
                float goalDist = Controllers.EuclideanDistance(diffPosition[0], diffPosition[1], 0, 0);
                float angleGoalPos = Controllers.Atan2d(diffPosition[1], diffPosition[0]);
                float angleDiff = angleGoalPos - ballControllingPlayerPos[2];

                /* If we are close enough to the goal, not very skewed, and no opponent is in front, shoot */
                if ((goalDist <= 20.0) && (Math.Abs(angleDiff) <= 60.0))
                {
                    if (verbose)
                        Console.WriteLine("                     YES!!!!!!!!!!!!!!!!");

                    return NodeStatus.SUCCESS;
                }
            }

            return NodeStatus.FAILURE;
        }
    }

    class IsOpponentBlocking : ConditionNode
    {
        public IsOpponentBlocking()
        {
            this.name = "IsOpponentBlocking";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            /* if there is another other team */
            if (data["team"].OppositeTeamPositions != null)
            {
                float[] pose = data["player"].Pose;
                for (int i = 0; i < data["team"].OppositeTeamPositions.GetLength(1); i++)
                {
                    if (Controllers.EuclideanDistance(data["team"].OppositeTeamPositions[0, i], data["team"].OppositeTeamPositions[1, i], data["player"].Pose[0], data["player"].Pose[1]) <= Controllers.opponentsClosenessThresh)
                    {
                        float[] diffPosition = { (pose[0] - data["team"].OppositeTeamPositions[0, i]), (pose[1] - data["team"].OppositeTeamPositions[1, i]) };
                        float angleOpponent = Controllers.Atan2d(diffPosition[1], diffPosition[0]);
                        float angleDiff = angleOpponent - pose[2];
                        if (angleDiff < Controllers.AngleDiffThresh)
                            return NodeStatus.SUCCESS;
                    }
                }
            }
            return NodeStatus.FAILURE;
        }
    }

    class HasRoleAttack : ConditionNode
    {
        public HasRoleAttack()
        {
            this.name = "HasRoleAttack";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            if (data["player"].Role == "Attack")
            {
                return NodeStatus.SUCCESS;
            }

            return NodeStatus.FAILURE;
        }
    }

    class HasRoleDefend : ConditionNode
    {
        public HasRoleDefend()
        {
            this.name = "HasRoleDefend";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            if (data["player"].Role == "Defend")
            {
                return NodeStatus.SUCCESS;
            }

            return NodeStatus.FAILURE;
        }
    }

    class HasRolePressing : ConditionNode
    {
        public HasRolePressing()
        {
            this.name = "HasRolePressing";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            if (data["player"].Role == "Pressing")
            {
                return NodeStatus.SUCCESS;
            }

            return NodeStatus.FAILURE;
        }
    }


    /* goalie nodes */
    class IsBallCatchable : ActionNode
    {
        /* only use for goalie!
         * returns SUCCESS if the ball is within the catchable area length
         * (not necessarily in the correct direction since the goalie can specify the angle when catching the ball) 
         * TODO: add goalie have to be inside penalty area! */
        public IsBallCatchable()
        {
            this.name = "IsBallCatchable";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            if (data["player"] != data["team"].Goalie)
                return NodeStatus.FAILURE;

            float[] goaliePos = data["player"].Pose;
            //Field field = new Field(data["team"].TeamSide);
            
            if (!data["team"].GameField.InTeamPenaltyArea(goaliePos))
                return NodeStatus.FAILURE;

            float catchable_l = 2.0F; // TODO: get from server
            float catchable_w = 1.0F;
            float cathcable_distance = Controllers.EuclideanDistance(catchable_w / 2, catchable_l, 0, 0);

            float[] ballPos = data["team"].GameBall.Position;
            float ballDist = Controllers.EuclideanDistance(goaliePos[0], goaliePos[1], ballPos[0], ballPos[1]);

            if (ballDist <= cathcable_distance)
                return NodeStatus.SUCCESS;
            return NodeStatus.FAILURE;
        }
    }

    class IsBallNotCatchable : ActionNode
    {
        /* only use for goalie! */
        public IsBallNotCatchable()
        {
            this.name = "IsBallNotCatchable";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            if (data["player"] != data["team"].Goalie)
                return NodeStatus.SUCCESS;

            float[] goaliePos = data["player"].Pose;
            //Field field = new Field(data["team"].TeamSide);

            if (!data["team"].GameField.InTeamPenaltyArea(goaliePos))
                return NodeStatus.SUCCESS;

            float catchable_l = 2.0F; // TODO: get from server
            float catchable_w = 1.0F;
            float cathcable_distance = Controllers.EuclideanDistance(catchable_w / 2, catchable_l, 0, 0);

            float[] ballPos = data["team"].GameBall.Position;
            float ballDist = Controllers.EuclideanDistance(goaliePos[0], goaliePos[1], ballPos[0], ballPos[1]);

            if (ballDist <= cathcable_distance)
                return NodeStatus.FAILURE;
            return NodeStatus.SUCCESS;
        }
    }
}
