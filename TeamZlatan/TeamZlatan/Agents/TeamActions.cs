﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamZlatan.BehaviorTrees;
using TeamZlatan.PlayingUtils;
using System.Timers;

namespace TeamZlatan.Agents
{

    class TeamPrepareForKickOff : ActionNode
    {
        public TeamPrepareForKickOff()
        {
            this.name = "TeamPrepareForKickOff";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            List<Player> teamPlayers = data["teamPlayers"];

            if ((data["team"].CurrentMode == "before_kick_off") || (data["team"].CurrentMode == "kick_off"))
            {
                teamPlayers[0].PositionInFormation = new float[2] { -50.0F, 0.0F };
                teamPlayers[0].Role = "StayInPosition";

                teamPlayers[1].PositionInFormation = new float[2] { -33.0F, -25.0F };
                teamPlayers[1].Role = "StayInPosition";
                teamPlayers[2].PositionInFormation = new float[2] { -37.0F, -10.0F };
                teamPlayers[2].Role = "StayInPosition";
                teamPlayers[3].PositionInFormation = new float[2] { -37.0F, 10.0F };
                teamPlayers[3].Role = "StayInPosition";
                teamPlayers[4].PositionInFormation = new float[2] { -33.0F, 25.0F };
                teamPlayers[4].Role = "StayInPosition";

                teamPlayers[5].PositionInFormation = new float[2] { -30.0F, 0.0F };
                teamPlayers[5].Role = "StayInPosition";
                teamPlayers[6].PositionInFormation = new float[2] { -20.0F, 15.0F };
                teamPlayers[6].Role = "StayInPosition";
                teamPlayers[7].PositionInFormation = new float[2] { -20.0F, -15.0F };
                teamPlayers[7].Role = "StayInPosition";

                teamPlayers[8].PositionInFormation = new float[2] { -10.0F, -20.0F };
                teamPlayers[8].Role = "StayInPosition";
                teamPlayers[9].PositionInFormation = new float[2] { -10.0F, 20.0F };
                teamPlayers[9].Role = "StayInPosition";

                if (data["team"].KickOffSide == data["team"].TeamSide)
                {
                    teamPlayers[10].PositionInFormation = new float[2] { 0.0F, 0.7F }; // can't be on exact ball position (will be thrown away!)
                    teamPlayers[10].Role = "KickOff";
                }
                else
                {
                    teamPlayers[10].PositionInFormation = new float[2] { -10.0F, 0.0F };
                    teamPlayers[10].Role = "StayInPosition";
                }

                foreach (Player p in data["teamPlayers"])
                {
                    if (!p.FormationSet)
                    {
                        return NodeStatus.RUNNING;
                    }
                }
            }

            return NodeStatus.SUCCESS;
        }
    }

    class SetDefensiveFormation : ActionNode
    {
        public SetDefensiveFormation()
        {
            this.name = "SetDefensiveFormation";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            List<Player> teamPlayers = data["teamPlayers"];

            teamPlayers[0].PositionInFormation = data["team"].GoalieBlockingPosition; ;
            teamPlayers[0].Role = "Defend";

            teamPlayers[1].PositionInFormation = new float[2] { -33.0F, -25.0F };
            teamPlayers[1].Role = "Defend";
            teamPlayers[2].PositionInFormation = new float[2] { -37.0F, -10.0F };
            teamPlayers[2].Role = "Defend";
            teamPlayers[3].PositionInFormation = new float[2] { -37.0F, 10.0F };
            teamPlayers[3].Role = "Defend";
            teamPlayers[4].PositionInFormation = new float[2] { -33.0F, 25.0F };
            teamPlayers[4].Role = "Defend";

            teamPlayers[5].PositionInFormation = new float[2] { -30.0F, 0.0F };
            teamPlayers[5].Role = "Defend";
            teamPlayers[6].PositionInFormation = new float[2] { -25.0F, 15.0F };
            teamPlayers[6].Role = "Defend";
            teamPlayers[7].PositionInFormation = new float[2] { -25.0F, -15.0F };
            teamPlayers[7].Role = "Defend";

            teamPlayers[8].PositionInFormation = new float[2] { -15.0F, -20.0F };
            teamPlayers[8].Role = "Defend";
            teamPlayers[9].PositionInFormation = new float[2] { -15.0F, 20.0F };
            teamPlayers[9].Role = "Defend";

            teamPlayers[10].PositionInFormation = new float[2] { -10.0F, 0.0F };
            teamPlayers[10].Role = "Defend";

            if (data["team"].TeamSide == 'r')
            {
                for (int i = 0; i < teamPlayers.Count; i++)
                {
                    teamPlayers[i].PositionInFormation[0] = teamPlayers[i].PositionInFormation[0] * -1;
                    teamPlayers[i].PositionInFormation[1] = teamPlayers[i].PositionInFormation[1] * -1;
                }
            }

            return NodeStatus.SUCCESS;
        }
    }

    class SetOffensiveFormation : ActionNode
    {
        public SetOffensiveFormation()
        {
            this.name = "SetOffensiveFormation";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            List<Player> teamPlayers = data["teamPlayers"];

            teamPlayers[0].PositionInFormation = data["team"].GoalieBlockingPosition;
            teamPlayers[0].Role = "Attack";

            teamPlayers[1].PositionInFormation = new float[2] { -23.0F, -25.0F };
            teamPlayers[1].Role = "Attack";
            teamPlayers[2].PositionInFormation = new float[2] { -27.0F, -10.0F };
            teamPlayers[2].Role = "Attack";
            teamPlayers[3].PositionInFormation = new float[2] { -27.0F, 10.0F };
            teamPlayers[3].Role = "Attack";
            teamPlayers[4].PositionInFormation = new float[2] { -23.0F, 25.0F };
            teamPlayers[4].Role = "Attack";

            teamPlayers[5].PositionInFormation = new float[2] { -10.0F, 0.0F };
            teamPlayers[5].Role = "Attack";
            teamPlayers[6].PositionInFormation = new float[2] { 0.0F, 15.0F };
            teamPlayers[6].Role = "Attack";
            teamPlayers[7].PositionInFormation = new float[2] { 0.0F, -15.0F };
            teamPlayers[7].Role = "Attack";

            teamPlayers[8].PositionInFormation = new float[2] { 20.0F, -20.0F };
            teamPlayers[8].Role = "Attack";
            teamPlayers[9].PositionInFormation = new float[2] { 20.0F, 20.0F };
            teamPlayers[9].Role = "Attack";

            teamPlayers[10].PositionInFormation = new float[2] { 30.0F, 0.0F };
            teamPlayers[10].Role = "Attack";

            //float sideOffset = 1;
            if (data["team"].TeamSide == 'r')
            {
                for(int i=0; i<teamPlayers.Count ;i++)
                {
                    teamPlayers[i].PositionInFormation[0] = teamPlayers[i].PositionInFormation[0] * -1;
                    teamPlayers[i].PositionInFormation[1] = teamPlayers[i].PositionInFormation[1] * -1;
                }
            }
                

            return NodeStatus.SUCCESS;
        }
    }

    class SetPressingFormation : ActionNode
    {
        public SetPressingFormation()
        {
            this.name = "SePressingFormation";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            List<Player> teamPlayers = data["teamPlayers"];

            if (data["team"].OppositeTeamPositions == null)
            {
                teamPlayers[0].PositionInFormation = data["team"].GoalieBlockingPosition;
                teamPlayers[0].Role = "Defend";

                teamPlayers[1].PositionInFormation = new float[2] { -33.0F, -25.0F };
                teamPlayers[1].Role = "Defend";
                teamPlayers[2].PositionInFormation = new float[2] { -37.0F, -10.0F };
                teamPlayers[2].Role = "Defend";
                teamPlayers[3].PositionInFormation = new float[2] { -37.0F, 10.0F };
                teamPlayers[3].Role = "Defend";
                teamPlayers[4].PositionInFormation = new float[2] { -33.0F, 25.0F };
                teamPlayers[4].Role = "Defend";

                teamPlayers[5].PositionInFormation = new float[2] { -30.0F, 0.0F };
                teamPlayers[5].Role = "Defend";
                teamPlayers[6].PositionInFormation = new float[2] { -20.0F, 15.0F };
                teamPlayers[6].Role = "Defend";
                teamPlayers[7].PositionInFormation = new float[2] { -20.0F, -15.0F };
                teamPlayers[7].Role = "Defend";

                teamPlayers[8].PositionInFormation = new float[2] { 5.0F, -20.0F };
                teamPlayers[8].Role = "Defend";
                teamPlayers[9].PositionInFormation = new float[2] { 5.0F, 20.0F };
                teamPlayers[9].Role = "Defend";

                teamPlayers[10].PositionInFormation = new float[2] { 10.0F, 0.0F };
                teamPlayers[10].Role = "Defend";

                if (data["team"].TeamSide == 'r')
                {
                    for (int i = 0; i < teamPlayers.Count; i++)
                    {
                        teamPlayers[i].PositionInFormation[0] = teamPlayers[i].PositionInFormation[0] * -1;
                        teamPlayers[i].PositionInFormation[1] = teamPlayers[i].PositionInFormation[1] * -1;
                    }
                }
            }
            else
            {
                for (int i = 1; i < teamPlayers.Count; i++)
                {
                    int idxEnemyAssigned = data["team"].MarkAssigned[teamPlayers[i]];
                    teamPlayers[i].PositionInFormation[0] = data["team"].OppositeTeamPositions[0, idxEnemyAssigned];
                    teamPlayers[i].PositionInFormation[1] = data["team"].OppositeTeamPositions[1, idxEnemyAssigned];
                }
            }

            return NodeStatus.SUCCESS;
        }
    }

    class Wait : ActionNode
    {
        /* wait node: waits for w number of seconds */
        private int waitSeconds;
        public Wait(int w)
        {
            this.name = "Wait";
            waitSeconds = w;
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            if (data["team"].Time == new DateTime(1, 1, 1))
            {
                data["team"].Time = DateTime.Now;
            }

            int elapsedSeconds = (DateTime.Now - data["team"].Time).Seconds;

            if (elapsedSeconds < waitSeconds)
                return NodeStatus.RUNNING;

            data["team"].Time = new DateTime(1, 1, 1); // reset time
            return NodeStatus.SUCCESS;
        }
    }

    class DoNothing : ConditionNode
    {
        public DoNothing()
        {
            this.name = "DoNothing";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            return NodeStatus.RUNNING;
        }
    }


    /* ------- Coach actions ------- */

    class KickOffGame : ActionNode
    {
        public KickOffGame()
        {
            this.name = "KickOffGame";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            if (data["team"].CurrentMode == "before_kick_off")
            {
                data["coach"].StartGame();
                return NodeStatus.SUCCESS;
            }

            return NodeStatus.FAILURE;
        }
    }
}
