﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Timers;
using TeamZlatan.Util;
using TeamZlatan.Agents;
using TeamZlatan.BehaviorTrees;

namespace TeamZlatan.Agents
{
    class Player : AbstractAgent
    {

        #region Attributes
        private int shirtNumber;
        private BehaviorTree bTree;
        private char teamSide;
        private float[] pose = { 0, 0, 0 };
        private float[] velocity = { 0, 0 };
        private float[] positionInFormation = { 0, 0 };
        private float[] goalPosition = { 0, 0 };

        private String role;
        private String lastCommandSent = null;

        private Team team;
        private bool formationSet = false;
        private bool hasBall = false;
        private int turnsLeft;
        private bool isOffside = false; // todo: set this variable depending on the state of the player

        private float headAngle;
        private int timeTranscurred;
        private float stamina;
        private float effort;
        private float amountOfSpeed;
        private float directionOfSpeed;
        private int kickCount;
        private int dashCount;
        private int turnCount;
        private int sayCount;
        private int turnNeckCount;
        private int catchCount;
        private int moveCount;
        private int changeViewCount;
        #endregion


        #region GettersSetters
        public int ShirtNumber { get { return shirtNumber; } set { shirtNumber = value; } }
        public char TeamSide { get { return teamSide; } set { teamSide = value; } }
        public BehaviorTree BTree { get { return bTree; } set { bTree = value; } }
        public bool HasBall { get { return hasBall; } set { hasBall = value; } }
        public float[] Pose { get { return pose; } set { pose = value; } }
        public float[] Velocity { get { return velocity; } set { velocity = value; } }
        public float[] PositionInFormation { get { return positionInFormation; } set { positionInFormation = value; } }
        public float[] GoalPosition { get { return goalPosition; } set { goalPosition = value; } }
        public bool FormationSet { get { return formationSet; } set { formationSet = value; } }
        public int TurnsLeft { get { return turnsLeft; } set { turnsLeft = value; } }
        public float HeadAngle { get { return headAngle; } set { headAngle = value; } }
        public int TimeTranscurred { get { return timeTranscurred; } set { timeTranscurred = value; } }
        public float Stamina { get { return stamina; } set { stamina = value; } }
        public float Effort { get { return effort; } set { effort = value; } }
        public float AmountOfSpeed { get { return amountOfSpeed; } set { amountOfSpeed = value; } }
        public float DirectionOfSpeed { get { return directionOfSpeed; } set { directionOfSpeed = value; } }
        public int KickCount { get { return kickCount; } set { kickCount = value; } }
        public int DashCount { get { return dashCount; } set { dashCount = value; } }
        public int TurnCount { get { return turnCount; } set { turnCount = value; } }
        public int SayCount { get { return sayCount; } set { sayCount = value; } }
        public int TurnNeckCount { get { return turnNeckCount; } set { turnNeckCount = value; } }
        public int CatchCount { get { return catchCount; } set { catchCount = value; } }
        public int MoveCount { get { return moveCount; } set { moveCount = value; } }
        public int ChangeViewCount { get { return changeViewCount; } set { changeViewCount = value; } }
        public Team Team { get { return team; } set { team = value; } }
        public string Role { get { return role; } set { role = value; } }
        public bool IsOffside { get { return isOffside; } set { isOffside = value; } }
        public string LastCommandSent { get { return lastCommandSent; } set { lastCommandSent = value; } }
        #endregion


        public Player(String playerType, String teamName, int serverPort)
        {
            this.playerType = playerType;
            this.teamName = teamName;
            this.serverConnection = new RCSSSConnection(serverPort);
            String reply = this.serverConnection.Send(this.InitializationMessage());
            ParseMessage(reply);

            System.Threading.Thread.Sleep(50);
            String initializationReply;
            while (this.serverConnection.DataAvailable())
            {
                initializationReply = this.serverConnection.Listen();
            }

            InitializeBehaviorTree();
        }


        public void InitializeBehaviorTree()
        {
            bTree = new BehaviorTree();
            TreeNode sequenceBeforeGameStart, sequenceKickOff;

            if (this.playerType == "goalie")
            {
                sequenceBeforeGameStart = new SequenceNode("SequenceBeforeGameStart");
                (sequenceBeforeGameStart as SequenceNode).Children = new TreeNode[]
                {
                    new IsBeforeKickOff(),
                    new IsInitFormationSet(),
                    new AchieveInitialFormation(),
                };

                sequenceKickOff = new SequenceNode("SequenceKickOff");
                (sequenceKickOff as SequenceNode).Children = new TreeNode[]
                {
                    new IsKickOff(),
                    new KickOff()
                };

                TreeNode sequenceCatch = new SequenceNode("SequenceCatch");
                (sequenceCatch as SequenceNode).Children = new TreeNode[]
                {
                    new IsPlayOn(),
                    new IsBallCatchable(),
                    new GoalieCatchBall(),
                    new ShootAtGoal()
                };

                TreeNode sequenceDefend = new SequenceNode("SequenceDefend");
                (sequenceDefend as SequenceNode).Children = new TreeNode[]
                {
                    new IsBallNotCatchable(),
                    new ArriveAtPosition()
                };

                TreeNode sequenceFreeKick = new SequenceNode("SequenceFreeKick");
                (sequenceFreeKick as SequenceNode).Children = new TreeNode[]
                {
                    new IsFreeKick(),
                    new ArriveAtPosition(),
                    //new GoalieMove(), // can move with ball.
                    //new PassToBestReceiver()
                    new ShootAtGoal()
                };

                TreeNode sequenceGoalKick = new SequenceNode("SequenceGoalKick");
                (sequenceGoalKick as SequenceNode).Children = new TreeNode[]
                {
                    new IsGoalKick(),
                    new ArriveAtPosition(),
                    //new PassToBestReceiver()
                    new ShootAtGoal()
                };

                TreeNode sequenceBallControlling = new SequenceNode("SequenceBallControlling");
                (sequenceBallControlling as SequenceNode).Children = new TreeNode[]
                {
                    new IsPlayOn(),
                    new IsBallControllingPlayer(),
                    new ShootAtGoal()
                };

                TreeNode rootSelector = bTree.Root.Child = new SelectorNode("SelectorAtRoot");
                (rootSelector as SelectorNode).Children = new TreeNode[]
                {
                    sequenceBeforeGameStart,
                    sequenceKickOff,
                    sequenceCatch,
                    sequenceGoalKick,
                    sequenceFreeKick,
                    sequenceBallControlling,
                    sequenceDefend
                };

            }
            else
            {

                /* Before kick off & kick off nodes */
                sequenceKickOff = new SequenceNode("SequenceKickOff");
                (sequenceKickOff as SequenceNode).Children = new TreeNode[]
                {
                    new IsKickOff(),
                    new KickOff()
                };

                sequenceBeforeGameStart = new SequenceNode("SequenceBeforeGameStart");
                (sequenceBeforeGameStart as SequenceNode).Children = new TreeNode[]
                {
                    new IsBeforeKickOff(),
                    new IsInitFormationSet(),
                    new AchieveInitialFormation(),
                };

                TreeNode sequenceFreeKick = new SequenceNode("SequenceFreeKick");
                (sequenceFreeKick as SequenceNode).Children = new TreeNode[]
                {
                    new IsFreeKick(),
                    new ShootAtGoal()
                };

                /* General behaviors (both attacking and defending) */
                TreeNode sequencePlayerClosestToBall = new SequenceNode("SequencePlayerClosestToBall");
                (sequencePlayerClosestToBall as SequenceNode).Children = new TreeNode[]
                {
                    new IsPlayerClosestToBall(),
                    new ArriveAtPosition(),
                };

                /* Attack nodes */
                TreeNode sequenceFreeDuringAttackPlayers = new SequenceNode("SequenceFreeDuringAttackPlayers");
                (sequenceFreeDuringAttackPlayers as SequenceNode).Children = new TreeNode[]
                 {
                    new ArriveAtPosition()
                 };

                TreeNode sequenceReceivingPlayers = new SequenceNode("SequenceReceivingPlayers");
                (sequenceReceivingPlayers as SequenceNode).Children = new TreeNode[]
                 {
                    new IsReceivingPlayer(),
                    new ArriveAtPosition()
                 };

                TreeNode sequenceSupportingPlayer = new SequenceNode("SequenceSupportingPlayer");
                (sequenceSupportingPlayer as SequenceNode).Children = new TreeNode[]
                 {
                    new IsSupportingPlayer(),
                    new ArriveAtPosition()
                 };

                TreeNode selectorPlayersWithoutBall = new SelectorNode("SelectorPlayersWithoutBall");
                (selectorPlayersWithoutBall as SelectorNode).Children = new TreeNode[]
                 {
                    sequencePlayerClosestToBall,
                    sequenceSupportingPlayer,
                    sequenceReceivingPlayers,
                    sequenceFreeDuringAttackPlayers
                 };

                TreeNode sequenceTikiTaka = new SequenceNode("SequenceTikiTaka");
                (sequenceTikiTaka as SequenceNode).Children = new TreeNode[]
                 {
                    new AreReceiversAround(),
                    new ArriveAtPosition(),
                    new PassToBestReceiver()
                 };

                TreeNode sequenceGoForward = new SequenceNode("SequenceGoForward");
                (sequenceGoForward as SequenceNode).Children = new TreeNode[]
                 {
                    new IsOpponentNotVeryClose(),
                    new ArriveAtPosition(),
                    new IsBallKickableTowardsGoal(),
                    new MoveBallForward()
                 };

                TreeNode sequenceThroughPass = new SequenceNode("SequenceThroughPass");
                (sequenceThroughPass as SequenceNode).Children = new TreeNode[]
                 {
                    new IsThroughPassPossible(),
                    new ArriveAtPosition(),
                    new PassSupportingPlayer()
                 };

                TreeNode sequenceShootAtGoal = new SequenceNode("SequenceShootAtGoal");
                (sequenceShootAtGoal as SequenceNode).Children = new TreeNode[]
                 {
                    new IsScoringPossible(),
                    new ArriveAtPosition(),
                    new ShootAtGoal()
                 };


                TreeNode sequenceEvadeOtherPlayers = new SequenceNode("sequenceEvadeOtherPlayers");
                (sequenceEvadeOtherPlayers as SequenceNode).Children = new TreeNode[]
                 {
                    new IsOtherPlayerCloseAndFacing(),
                    new MoveBallForward(),
                    new ArriveAtPosition()
                 };

                TreeNode selectorBallControlBehaviors = new SelectorNode("SelectorBallControlBehaviors");
                (selectorBallControlBehaviors as SelectorNode).Children = new TreeNode[]
                 {
                    sequenceShootAtGoal,
                    sequenceThroughPass,
                    sequenceGoForward,
                    sequenceTikiTaka,
                    sequenceEvadeOtherPlayers
                 };

                TreeNode sequenceBallControl = new SequenceNode("SequenceBallControl");
                (sequenceBallControl as SequenceNode).Children = new TreeNode[]
                 {
                    new IsBallControllingPlayer(),
                    selectorBallControlBehaviors
                 };

                TreeNode selectorAttack = new SelectorNode("SelectorAttack");
                (selectorAttack as SelectorNode).Children = new TreeNode[]
                {
                    sequenceBallControl,
                    selectorPlayersWithoutBall
                };



                /* Root node */
                TreeNode rootSelector = bTree.Root.Child = new SelectorNode("SelectorAtRoot");
                (rootSelector as SelectorNode).Children = new TreeNode[]
                {
                    sequenceBeforeGameStart,
                    sequenceKickOff,
                    sequenceFreeKick,
                    selectorAttack
                    //selectorDefend
                };
            }
        }


        public void RunBehaviorTree()
        {
            Console.WriteLine("Player type:" + this.playerType + " with number " + this.shirtNumber);
            AskServerAboutPlayerStatus();

            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>();

            data["player"] = this;
            data["team"] = this.team;
            bTree.Update(data);
        }


        public void AskServerAboutPlayerStatus()
        {
            String sensorInformation;
            while (this.serverConnection.DataAvailable())
            {
                sensorInformation = this.serverConnection.Listen();
                //ParseMessage(sensorInformation);
                this.team.ParseMessage(sensorInformation); 
            }
            //String senseBodyReply = this.SenseBody();
            //ParseMessage(senseBodyReply);
        }




        #region MessagesToServer
        public String InitializationMessage()
        {
            String message = "(init " + this.teamName + "(" + this.RCSSSversion + ")";
            if (this.playerType == "goalie")
            {
                message = message + " (goalie))";
            }
            else
            {
                message = message + ")";
            }
            return message;
        }
        public String SenseBody()
        {
            String reply = this.serverConnection.Send("(sense_body)\0");
            if (verboseMessages)
            {
                Console.WriteLine("                    Sense body");
                Console.WriteLine(reply);
            }
            //this.team.ParseMessage(reply, true);
            return reply;
        }

        public void Move(float xPos, float yPos)
        {
            // can only be used before kick off (xPos: -52.5 to 0, yPos: -34 to 34
            String reply = this.serverConnection.Send("(move " + xPos.ToString() + " " + yPos.ToString() + ")\0");
            if (verboseMessages)
            {
                Console.WriteLine("                    Dash");
                Console.WriteLine(reply);
            }
            //this.team.ParseMessage(reply, true);
            lastCommandSent = "move";
        }

        public void Dash(float xPos, float yPos)
        {
            // don't know why this works... (dash should only have one param, see dash_forward)
            String reply = this.serverConnection.Send("(dash " + xPos.ToString() + " " + yPos.ToString() + ")\0");
            if (verboseMessages)
            {
                Console.WriteLine("                    Dash");
                Console.WriteLine(reply);
            }
            //this.team.ParseMessage(reply, true);
            lastCommandSent = "dash";
        }

        public void DashForward(float power)
        {
            // move in forward direction
            String reply = this.serverConnection.Send("(dash " + power.ToString() + ")\0");
            if (verboseMessages)
            {
                Console.WriteLine("                    Dash forward");
                Console.WriteLine(reply);
            }
            //this.team.ParseMessage(reply, true);
            lastCommandSent = "dashForward";
        }

        public void Turn(float theta)
        {
            // theta: -180 to 180
            String reply = this.serverConnection.Send("(turn " + theta.ToString() + ")\0");
            //Console.WriteLine("(turn " + theta.ToString() + ")\0");
            if (verboseMessages)
            {
                Console.WriteLine("                    Turn");
                Console.WriteLine(reply);
            }
            //this.team.ParseMessage(reply, true);
            lastCommandSent = "turn";
        }

        public void TurnNeck(float theta)
        {
            // theta: -90 to 90
            String reply = this.serverConnection.Send("(turn_neck " + theta.ToString() + ")\0");
            if (verboseMessages)
            {
                Console.WriteLine("                    Turn neck");
                Console.WriteLine(reply);
            }
            //this.team.ParseMessage(reply, true);
            lastCommandSent = "turn";
        }

        public void Kick(float power, float theta)
        {
            String reply = this.serverConnection.Send("(kick " + power.ToString() + " " + theta.ToString() + ")\0");
            if (verboseMessages)
            {
                Console.WriteLine("                    Kick");
                Console.WriteLine(reply);
            }
            //this.team.ParseMessage(reply, true);
            lastCommandSent = "kick";
        }

        public void CatchBall(float theta)
        {
            String reply = this.serverConnection.Send("(catch " + theta.ToString() + ")\0");
            if (verboseMessages)
            {
                Console.WriteLine("                    Catch");
                Console.WriteLine(reply);
            }
            //this.team.ParseMessage(reply, true);
            lastCommandSent = "catch";
        }

        public String EarOn()
        {
            String reply = this.serverConnection.Send("(ear on)\0");
            
            if (verboseMessages)
            {
                Console.WriteLine("                    Ear on");
                Console.WriteLine(reply);
            }
            lastCommandSent = "earOn";

            return reply;
        }

        public String EarOff()
        {
            String reply = this.serverConnection.Send("(ear off)\0");

            if (verboseMessages)
            {
                Console.WriteLine("                    Ear off");
                Console.WriteLine(reply);
            }
            lastCommandSent = "earOff";

            return reply;
        }

        public String EyeOn()
        {
            String reply = this.serverConnection.Send("(eye on)\0");

            if (verboseMessages)
            {
                Console.WriteLine("                    Eye on");
                Console.WriteLine(reply);
            }
            lastCommandSent = "eyeOn";

            return reply;
        }

        public String EyeOff()
        {
            String reply = this.serverConnection.Send("(eye off)\0");

            if (verboseMessages)
            {
                Console.WriteLine("                    Eye off");
                Console.WriteLine(reply);
            }
            lastCommandSent = "eyeOff";

            return reply;
        }

        public void Bye()
        {
            String reply = this.serverConnection.Send("(bye)\0");
            if (verboseMessages)
            {
                Console.WriteLine("                    Bye");
                Console.WriteLine(reply);
            }
        }
        #endregion


        #region MessageParsing
        public void ParseMessage(String message)
        {
            if ((message == null) || (message.Length < 10))
                return;

            if (message.Substring(1, 12) == "player_param")
            {
                //Console.WriteLine("-- player_param --");
                // parse player param message
            }
            else if (message.Substring(1, 11) == "player_type")
            {
                //Console.WriteLine("-- player_type --");
                // parse player type message
            }
            else if (message.Substring(1, 10) == "sense_body")
            {
                //Console.WriteLine("-- sense_body --");
                message = message.Substring(12, message.Length - 12);

                int numDigits = NumberOfIntegerDigitsString(message);
                int.TryParse(message.Substring(0, numDigits), out this.timeTranscurred);
                message = message.Substring(numDigits + 11, message.Length - numDigits - 11);
#pragma warning disable CS0219 // La variable 'viewWidth' está asignada pero su valor nunca se usa
#pragma warning disable CS0219 // La variable 'viewQuality' está asignada pero su valor nunca se usa
                String viewQuality, viewWidth;
#pragma warning restore CS0219 // La variable 'viewQuality' está asignada pero su valor nunca se usa
#pragma warning restore CS0219 // La variable 'viewWidth' está asignada pero su valor nunca se usa
                if (message.Substring(0, 4) == "high")
                {
                    viewQuality = "high";
                    message = message.Substring(5, message.Length - 5);
                }
                else
                {
                    viewQuality = "low";
                    message = message.Substring(4, message.Length - 4);
                }

                if (message.Substring(0, 4) == "wide")
                {
                    viewWidth = "low";
                    message = message.Substring(5 + 10, message.Length - 5 - 10);
                }
                else if (message.Substring(0, 6) == "normal")
                {
                    viewWidth = "normal";
                    message = message.Substring(7 + 10, message.Length - 7 - 10);
                }
                else
                {
                    viewWidth = "narrow";
                    message = message.Substring(7 + 10, message.Length - 7 - 10);
                }


                numDigits = NumberOfFloatDigitsString(message);
                float.TryParse(message.Substring(0, numDigits), out this.stamina);
                message = message.Substring(numDigits + 2, message.Length - numDigits - 2);

                numDigits = NumberOfFloatDigitsString(message);
                float.TryParse(message.Substring(0, numDigits), out this.effort);
                message = message.Substring(numDigits + 9, message.Length - numDigits - 9);

                numDigits = NumberOfFloatDigitsString(message);
                float.TryParse(message.Substring(0, numDigits), out this.amountOfSpeed);
                message = message.Substring(numDigits, message.Length - numDigits);

                numDigits = NumberOfFloatDigitsString(message);
                float.TryParse(message.Substring(0, numDigits), out this.directionOfSpeed);
                message = message.Substring(numDigits + 11, message.Length - numDigits - 11);

                numDigits = NumberOfFloatDigitsString(message);
                float.TryParse(message.Substring(0, numDigits), out this.headAngle);
                message = message.Substring(numDigits + 8, message.Length - numDigits - 8);

                numDigits = NumberOfIntegerDigitsString(message);
                int.TryParse(message.Substring(0, numDigits), out this.kickCount);
                message = message.Substring(numDigits + 8, message.Length - numDigits - 8);

                numDigits = NumberOfIntegerDigitsString(message);
                int.TryParse(message.Substring(0, numDigits), out this.dashCount);
                message = message.Substring(numDigits + 8, message.Length - numDigits - 8);

                numDigits = NumberOfIntegerDigitsString(message);
                int.TryParse(message.Substring(0, numDigits), out this.turnCount);
                message = message.Substring(numDigits + 7, message.Length - numDigits - 7);

                numDigits = NumberOfIntegerDigitsString(message);
                int.TryParse(message.Substring(0, numDigits), out this.sayCount);
                message = message.Substring(numDigits + 13, message.Length - numDigits - 13);

                numDigits = NumberOfIntegerDigitsString(message);
                int.TryParse(message.Substring(0, numDigits), out this.turnNeckCount);
                message = message.Substring(numDigits + 9, message.Length - numDigits - 9);

                numDigits = NumberOfIntegerDigitsString(message);
                int.TryParse(message.Substring(0, numDigits), out this.catchCount);
                message = message.Substring(numDigits + 8, message.Length - numDigits - 8);

                numDigits = NumberOfIntegerDigitsString(message);
                int.TryParse(message.Substring(0, numDigits), out this.moveCount);
                message = message.Substring(numDigits + 15, message.Length - numDigits - 15);

                numDigits = NumberOfIntegerDigitsString(message);
                int.TryParse(message.Substring(0, numDigits), out this.changeViewCount);
            }
            else if (message.Substring(1, 3) == "see")
            {
                //Console.WriteLine("-- see --");
                // parse see message (visual sensor)
                // see format: (see Time ObjInfo ObjInfo . . . )
                // ObjInfo format: (ObjName Distance Direction [DistChange DirChange [BodyFacingDir HeadFacingDir]])
                // ObjName format: (p [TeamName [Unum]]) OR (b) OR (f FlagInfo) OR (g Side)
                // 1st param: p=player, b=ball, f=flag, g=goal, P=???
                // other params: l/r=side, t=top, c=center, b=bottom
                // the amount of information depends on the distance to the objects

                if (message.Contains("(b)"))
                {
                    //Console.WriteLine("Sees ball!!!");
                    //int distToBall = ..
                    //int dirToBall = ..
                }

            }
            else if (message.Substring(1, 4) == "init")
            {
                this.teamSide = message.ToCharArray()[6];
                bool twoDigits = int.TryParse(message.Substring(8, 2), out this.shirtNumber);
                if (twoDigits == false)
                {
                    int.TryParse(message.Substring(8, 1), out this.shirtNumber);
                }
            }

            //Console.WriteLine(message + "\n");
        }
        public int NumberOfFloatDigitsString(String message)
        {
            float x;
            int numDigits = 0;
            for (int i = 0; i < message.Length; i++)
            {
                if (float.TryParse(message.Substring(0, 1 + i), out x))
                {
                    numDigits++;
                }
                else
                {
                    break;
                }
            }

            return numDigits;
        }

        public int NumberOfIntegerDigitsString(String message)
        {
            int x, numDigits = 0;
            for (int i = 0; i < message.Length; i++)
            {
                if (int.TryParse(message.Substring(0, 1 + i), out x))
                {
                    numDigits++;
                }
                else
                {
                    break;
                }
            }

            return numDigits;
        }
#endregion

    }
}
