﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Timers;
using TeamZlatan.BehaviorTrees;
using TeamZlatan.PlayingUtils;
using TeamZlatan.Util;


namespace TeamZlatan.Agents
{
    class AchieveInitialFormation : ActionNode
    {
        public AchieveInitialFormation()
        {
            this.name = "AchieveInitialFormation";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            if (data["player"].FormationSet == false)
            {
                data["player"].Move(data["player"].PositionInFormation[0], data["player"].PositionInFormation[1]);
                data["player"].Pose[0] = data["player"].PositionInFormation[0];
                data["player"].Pose[1] = data["player"].PositionInFormation[1];
                data["player"].FormationSet = true;
                //return NodeStatus.RUNNING;
            }

            return NodeStatus.SUCCESS;
        }

    }

    class StayPut : ActionNode
    {
        public StayPut()
        {
            this.name = "StayPut";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            return NodeStatus.SUCCESS;
        }
    }

    class KickBall : ActionNode
    {
        public KickBall()
        {
            this.name = "KickBall";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            data["player"].Kick(50, 0);
            //data["team"].GameBall.PredictStoppingPosition(50, 0+data["player"].Pose[2]);
            return NodeStatus.SUCCESS;
        }
    }

    class KickOff : ActionNode
    {
        public KickOff()
        {
            this.name = "KickOff";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            Player player = data["player"];
            if ((player.Role == "KickOff") && (data["team"].CurrentMode == "kick_off"))
            {
                float angleDiff = 0;
                if (data["team"].TeamSide == 'l')
                {
                    angleDiff = 180 - player.Pose[2];
                }
                else
                {
                    angleDiff = 0 - player.Pose[2];
                }

                float[] pose = this.data["player"].Pose;
                float[] ballPosition = this.data["team"].GameBall.Position;
                double dist = Controllers.EuclideanDistance(pose[0], pose[1], ballPosition[0], ballPosition[1]);
                if (dist > 2.5)
                {
                    data["player"].FormationSet = false;
                    //Console.WriteLine("                player NOT in position ");
                }
                else
                {
                    data["player"].Kick(75, angleDiff);
                    return NodeStatus.SUCCESS;
                }
                return NodeStatus.RUNNING;

                // old version:
                //float angleDiff = 0;
                //if (data["team"].TeamSide == 'l')
                //{
                //    angleDiff = 180 - player.Pose[2];
                //}
                //else
                //{
                //    angleDiff = 0 - player.Pose[2];
                //}

                //if (Math.Abs(angleDiff) > Controllers.AngleDiffThresh)
                //{
                //    player.Turn(angleDiff); //TODO: fix this angle!
                //}
                //else
                //{
                //    float[] pose = this.data["player"].Pose;
                //    float[] ballPosition = this.data["team"].GameBall.Position;
                //    double dist = Controllers.EuclideanDistance(pose[0], pose[1], ballPosition[0], ballPosition[1]);
                //    if (dist > 2.5)
                //    {
                //        data["player"].FormationSet = false;
                //        //Console.WriteLine("                player NOT in position ");
                //    }
                //    else
                //    {
                //        data["player"].Kick(75, 0);
                //    }

                //}

                //// check if ball has moved after the kick (TODO: could also check if game mode has changed)
                //float[] ballPos = data["team"].GameBall.Position;
                //if (ballPos[0] != 0 && ballPos[1] != 0)
                //{
                //    //player.Team.CurrentMode = "play_on";
                //    return NodeStatus.SUCCESS;
                //}
            }

            return NodeStatus.RUNNING;
        }
    }

    class ArriveAtPosition : ActionNode
    {
        public ArriveAtPosition()
        {
            this.name = "ArriveAtPosition";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            float[] pose = this.data["player"].Pose;
            float[] goalPosition = this.data["player"].GoalPosition;

            float[] diffPosition = { (goalPosition[0] - pose[0]), (goalPosition[1] - pose[1]) };
            float posDist = Controllers.EuclideanNorm(diffPosition[0], diffPosition[1]);

            float angleGoalPos = Controllers.Atan2d(diffPosition[1], diffPosition[0]);
            float angleDiff = angleGoalPos - pose[2];

            if (angleDiff > 180.0)
                angleDiff = angleDiff - 360.0F;
            else if (angleDiff < -180.0)
                angleDiff = angleDiff + 360.0F;

            if (Math.Abs(angleDiff) < Controllers.AngleDiffThresh)
            {
                if (posDist < Controllers.DistThresh)
                {
                    //Console.WriteLine("Arrived at position!!!");
                    return NodeStatus.SUCCESS;
                }
                else
                {
                    float orientedDist = (float)(Controllers.Cosd(pose[2]) * diffPosition[0] + Controllers.Sind(pose[2]) * diffPosition[1]);
                    float dashCommand = Controllers.Kp_arrive * orientedDist;

                    if(data["player"]==data["team"].Goalie)
                        dashCommand = 2.0F * Controllers.Kp_arrive * orientedDist; // could be: if ball is close - move fast, otherwise slowly

                    //data["player"].DashForward(80 + dashCommand);
                    data["player"].DashForward(100);
                }
            }
            else
            {
                data["player"].Turn(angleDiff);
            }

            return NodeStatus.RUNNING;
        }
    }

    class PassSupportingPlayer : ActionNode
    {
        public PassSupportingPlayer()
        {
            this.name = "PassSupportingPlayer";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            if (data["player"] == data["team"].BallControllingPlayer)
            {
                float[] ballControllingPlayerPos = this.data["player"].Pose;
                float[] supportingPlayerPos = this.data["team"].SupportingPlayer.Pose;
                float[] diffPosition = { (supportingPlayerPos[0] - ballControllingPlayerPos[0]), (supportingPlayerPos[1] - ballControllingPlayerPos[1]) };
                float angleGoalPos = Controllers.Atan2d(diffPosition[1], diffPosition[0]);
                float angleDiff = angleGoalPos - ballControllingPlayerPos[2];

                float kickPower = data["team"].GameBall.PredictPowerToReachPos(supportingPlayerPos, angleDiff);
                data["player"].Kick(kickPower, angleDiff);
                //data["team"].GameBall.PredictStoppingPosition(kickPower, 0 + data["player"].Pose[2]);
                Sounds.Passing();
                return NodeStatus.SUCCESS;
            }

            return NodeStatus.FAILURE;
            
        }
    }

    class PassToBestReceiver : ActionNode
    {
        public PassToBestReceiver()
        {
            this.name = "PassToBestReceiver";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            /* Find receiving player that is closest to the goal. Pass to him */
            TikiTakaSpotCalculator tikitakaSpotCalculator = new TikiTakaSpotCalculator(data["team"].ReceivingPlayers);
            SupportSpot bestSpot = tikitakaSpotCalculator.DetermineBestTikiTakaSpot(data["team"]);
            Player bestReceiver = data["team"].ReceivingPlayers[0];

            for(int i=0; i< data["team"].ReceivingPlayers.Count ;i++)
            {
                if ((data["team"].ReceivingPlayers[i].Pose[0] == bestSpot.SpotPos[0]) && (data["team"].ReceivingPlayers[i].Pose[1] == bestSpot.SpotPos[1]))
                {
                    bestReceiver = data["team"].ReceivingPlayers[i];
                }
            }

            float[] ballControllingPlayerPos = this.data["player"].Pose;
            float[] bestReceiverPos = bestReceiver.Pose;
            float[] diffPosition = { (bestReceiverPos[0] - ballControllingPlayerPos[0]), (bestReceiverPos[1] - ballControllingPlayerPos[1]) };
            float angleGoalPos = Controllers.Atan2d(diffPosition[1], diffPosition[0]);
            float angleDiff = angleGoalPos - ballControllingPlayerPos[2];

            float kickPower = data["team"].GameBall.PredictPowerToReachPos(bestReceiverPos, angleDiff);

            data["player"].Kick(kickPower, angleDiff);
            //data["team"].GameBall.PredictStoppingPosition(kickPower, 0 + data["player"].Pose[2]);
            Sounds.Passing();
            return NodeStatus.SUCCESS;

            //return NodeStatus.FAILURE;

        }
    }

    class ShootAtGoal : ActionNode
    {
        public ShootAtGoal()
        {
            this.name = "ShootAtGoal";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            if (data["player"] == data["team"].BallControllingPlayer)
            {
                float[] ballControllingPlayerPos = this.data["player"].Pose;
                float[] goalPos = this.data["team"].OpponentGoal;

                float[] diffPosition = { (goalPos[0] - ballControllingPlayerPos[0]), (goalPos[1] - ballControllingPlayerPos[1]) };
                //float goalDist = Controllers.EuclideanDistance(diffPosition[0], diffPosition[1], 0, 0);
                float angleGoalPos = Controllers.Atan2d(diffPosition[1], diffPosition[0]);
                float angleDiff = angleGoalPos - ballControllingPlayerPos[2];

                data["player"].Kick(100, angleDiff);
                Sounds.GoalScream();
                return NodeStatus.SUCCESS;
            }

            return NodeStatus.FAILURE;
        }
    }

    class MoveBallForward : ActionNode
    {
        public MoveBallForward()
        {
            this.name = "MoveBallForward";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            if (data["player"] == data["team"].BallControllingPlayer)
            {
                float[] ballControllingPlayerPos = data["player"].Pose;
                float[] goalPos = data["player"].GoalPosition;

                float[] diffPosition = { (goalPos[0] - ballControllingPlayerPos[0]), (goalPos[1] - ballControllingPlayerPos[1]) };
                float angleGoalPos = Controllers.Atan2d(diffPosition[1], diffPosition[0]);
                float angleDiff = angleGoalPos - ballControllingPlayerPos[2];

                data["player"].Kick(15, angleDiff);
                return NodeStatus.SUCCESS;
            }

            return NodeStatus.FAILURE;
        }
    }



    /* goalie nodes */

    class GoalieCatchBall : ActionNode
    {
        /* only use for the goalie! */
        public GoalieCatchBall()
        {
            this.name = "GoalieCatchBall";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            if (data["player"] != data["team"].Goalie)
                return NodeStatus.FAILURE;

            if (data["team"].TriedCatchAtCycle == -1)
            {
                // get angle to cacth ball in
                float[] goaliePos = data["player"].Pose;
                float[] ballPos = data["team"].GameBall.Position;
                float[] diffPosition = { (goaliePos[0] - ballPos[0]), (goaliePos[1] - ballPos[1]) };
                float angleGoalPos = Controllers.Atan2d(diffPosition[1], diffPosition[0]);
                float angleDiff = angleGoalPos - goaliePos[2];
                data["player"].CatchBall(angleDiff); // Try to catch the ball in the given direction relative to the body direction
                data["team"].TriedCatchAtCycle = data["team"].Cycle;

                Sounds.LookOut();
                return NodeStatus.RUNNING;
            }

            // check if the ball was cacthed
            if (data["team"].CurrentMode == "goalie_catch_ball")
            {
                data["team"].TriedCatchAtCycle = -1; // reset value
                return NodeStatus.SUCCESS;
            }
            else if (data["team"].Cycle > data["team"].TriedCatchAtCycle)
                return NodeStatus.FAILURE; // next cycle and playing mode didn't change = failed

            return NodeStatus.RUNNING; // still same cycle
        }
    }

    class GoalieMove : ActionNode
    {
        /* only use for the goalie! 
         * use during free_kick mode: 
         * goalie can use the move command to move with the ball inside the penalty area */
        public GoalieMove()
        {
            this.name = "GoalieMove";
        }

        override
        public NodeStatus Tick()
        {
            Announce();

            Player goalie = data["player"];
            if (goalie != data["team"].Goalie)
                return NodeStatus.FAILURE;

            
            float[] moveToPos = data["team"].GameField.MidFrontTeamPenaltyArea();
            goalie.Move(moveToPos[0], moveToPos[1]);

            return NodeStatus.SUCCESS;
        }
    }
}

