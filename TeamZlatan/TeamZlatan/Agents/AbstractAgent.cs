﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamZlatan.Util;

namespace TeamZlatan.Agents
{
    abstract class AbstractAgent
    {
        protected RCSSSConnection serverConnection;
        protected String playerType;
        protected String teamName;
        protected String RCSSSversion = "version 15";
        protected bool verboseMessages = false;


        public RCSSSConnection ServerConnection { get { return serverConnection; } set { serverConnection = value; } }
        public string PlayerType { get { return playerType; } set { playerType = value; } }
        public string TeamName { get { return teamName; } set { teamName = value; } }

        public AbstractAgent() { }
    }
}
