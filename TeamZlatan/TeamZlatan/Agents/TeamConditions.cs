﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamZlatan.BehaviorTrees;
using TeamZlatan.PlayingUtils;

namespace TeamZlatan.Agents
{
    class IsPlayOn : ConditionNode
    {
        public IsPlayOn()
        {
            this.name = "IsPlayOn";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            //Console.WriteLine("Current play mode:" + data["team"].CurrentMode);
            if (data["team"].CurrentMode == "play_on")
            {
                return NodeStatus.SUCCESS;
            }
            return NodeStatus.FAILURE;
        }
    }

    class IsTeamReady : ConditionNode
    {
        public IsTeamReady()
        {
            this.name = "IsTeamReady";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            
            foreach(Player p in data["team"].TeamPlayers)
            {
                if(p.FormationSet == false)
                {
                    return NodeStatus.FAILURE;
                }
            }
            return NodeStatus.SUCCESS;
        }
    }

    class IsOpponentReady : ConditionNode
    {
        public IsOpponentReady()
        {
            this.name = "IsOpponentReady";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            if (data["team"].OppositeTeamPositions != null)
            {
                return NodeStatus.SUCCESS;
            }
            return NodeStatus.RUNNING;
        }
    }

    class IsBeforeKickOff : ConditionNode
    {
        public IsBeforeKickOff()
        {
            this.name = "IsBeforeKickOff";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            //Console.WriteLine("                Current play mode:" + data["team"].CurrentMode);
            if ((data["team"].CurrentMode == "before_kick_off"))
            {
                return NodeStatus.SUCCESS;
            }
            return NodeStatus.FAILURE;
        }
    }

    class IsKickOff : ConditionNode
    {
        public IsKickOff()
        {
            this.name = "IsKickOff";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            //Console.WriteLine("                Current play mode:" + data["team"].CurrentMode);
            if (data["team"].CurrentMode == "kick_off")
            {
                return NodeStatus.SUCCESS;
            }
            return NodeStatus.FAILURE;
        }
    }

    class IsFreeKick : ConditionNode
    {
        public IsFreeKick()
        {
            this.name = "IsFreeKick";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            //Console.WriteLine("                Current play mode:" + data["team"].CurrentMode);
            if (data["team"].CurrentMode == "free_kick")
            {
                return NodeStatus.SUCCESS;
            }
            return NodeStatus.FAILURE;
        }
    }

    class DoesEnemyHaveBall : ConditionNode
    {
        public DoesEnemyHaveBall()
        {
            this.name = "DoesEnemyHaveBall";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            // check if not any of our players is controlling the ball
            if (data["team"].BallControllingPlayer == null)
            {
                //Console.WriteLine("--- no ball controller in our team ---");
                if (data["team"].OppositeTeamPositions == null)
                    return NodeStatus.FAILURE;

                Ball ball = data["team"].GameBall;

                for (int i = 0; i < data["team"].OppositeTeamPositions.GetLength(1); i++)
                {
                    float xpos = data["team"].OppositeTeamPositions[0, i];
                    float ypos = data["team"].OppositeTeamPositions[1, i];
                    float playerBallDist = Controllers.EuclideanDistance(xpos, ypos, ball.Position[0], ball.Position[1]);

                    if (playerBallDist < Controllers.ballControlDistance)
                        return NodeStatus.SUCCESS;
                }
            }
            //Console.WriteLine("--- our team controlls the ball ---");

            return NodeStatus.FAILURE;
        }
    }

    class IsBallOnTeamSide : ConditionNode
    {
        public IsBallOnTeamSide()
        {
            this.name = "IsBallOnTeamSide";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            Ball ball = data["team"].GameBall;
            if (ball.GetSide() == data["team"].TeamSide)
                return NodeStatus.SUCCESS;

            return NodeStatus.FAILURE;
        }
    }

    class IsBallOnEnemySide : ConditionNode
    {
        public IsBallOnEnemySide()
        {
            this.name = "IsBallOnEnemySide";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            Ball ball = data["team"].GameBall;
            if (ball.GetSide() != data["team"].TeamSide)
                return NodeStatus.SUCCESS;

            return NodeStatus.FAILURE;
        }
    }

    class DoesOurTeamHaveBall : ConditionNode
    {
        public DoesOurTeamHaveBall()
        {
            this.name = "DoesOurTeamHaveBall";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            Ball ball = data["team"].GameBall;
            if (data["team"].BallControllingPlayer != null)
                return NodeStatus.SUCCESS;

            return NodeStatus.FAILURE;
        }
    }

    class IsGoalKick : ConditionNode
    {
        public IsGoalKick()
        {
            this.name = "IsBeforeKickOff";
        }

        override
        public NodeStatus Tick()
        {
            Announce();
            //Console.WriteLine("                Current play mode:" + data["team"].CurrentMode);
            if ((data["team"].CurrentMode == "goal_kick"))
            {
                return NodeStatus.SUCCESS;
            }
            return NodeStatus.FAILURE;
        }
    }
}
