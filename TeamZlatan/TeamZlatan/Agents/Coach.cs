﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamZlatan.Util;

namespace TeamZlatan.Agents
{
    class Coach : AbstractAgent
    {
        #region Attributes
        private bool isOnline = true;
        #endregion

        #region GettersSetters
        public bool IsOnline { get { return isOnline; } set { isOnline = value; } }
#endregion

        public Coach(String teamName, int offlineCoachPort, int onlineCoachPort, bool online=true)
        {
            this.TeamName = teamName;
            if (online)
            {
                this.serverConnection = new RCSSSConnection(onlineCoachPort);
                this.serverConnection.Send("(init " + this.TeamName + " (" + this.RCSSSversion + "))\0");
            }
            else
            {
                this.serverConnection = new RCSSSConnection(offlineCoachPort);
                this.serverConnection.Send("(init " + "(" + this.RCSSSversion + "))\0");
            }

            System.Threading.Thread.Sleep(50);
            //Console.WriteLine("Coach initialization reply");
            String initializationReply;
            while (this.serverConnection.DataAvailable())
            {
                initializationReply = this.serverConnection.Listen();
                //Console.WriteLine(initializationReply);
            }
            //Console.WriteLine("ok bye");
            //Console.ReadKey();

            this.isOnline = online;
        }
        

        #region MessagesToServer

        public bool IncomingMessage()
        {
            return serverConnection.DataAvailable();
        }


        public String EyeOn()
        {
            return this.serverConnection.Send("(eye on)");
        }


        public String EyeOff()
        {
            return this.serverConnection.Send("(eye off)");
        }

        public String EarOn()
        {
            return this.serverConnection.Send("(ear on)");
        }

        public String EarOff()
        {
            return this.serverConnection.Send("(ear off)");
        }

        public String Hear()
        {
            return this.serverConnection.Listen();
        }

        public String LookAround()
        {
            // request to get noise free information about the scene
            return this.serverConnection.Send("(look)");
        }

        public String StartGame()
        {
            return this.serverConnection.Send("(start)");
        }

        public String ChangeEyeMode(bool on)
        {
            /* If (eye on) is sent, the server starts sending (see global. . . ) information every 100 ms.
             * If (eye off) is sent, the server stops to send visual information automatically. In this case the trainer/coach has to ask actively with (look), if it needs visual information. */
            if (on)
                return this.serverConnection.Send("(eye on)");
            return this.serverConnection.Send("(eye off)");
        }

        public String ChangeEarMode(bool on)
        {
            /* If (ear on) is sent, the server sends all auditory information to the trainer.
               If (eye off) is sent, the server stops sending auditory information to the trainer. */
            if (on)
                return this.serverConnection.Send("(ear on)");
            return this.serverConnection.Send("(ear off)");
        }

        public String GetTeamNames()
        {
            return this.serverConnection.Send("(team_names)");
        }

        public String ChangePlayMode(String mode)
        {
            /* mode: before_kick_off for example */
            return this.serverConnection.Send("(change_mode " + mode + ")"); 
        }

        public String CheckBall()
        {
            /* returns the position of the ball: in_field / goal_r / goal_l / out_of_field */
            return this.serverConnection.Send("(check_ball)");
        }

        public String ResetPlayerCapacity()
        {
            /* This command resets players' stamina, recovery, effort and hear capacity to the values at the beginning of the game. */
            return this.serverConnection.Send("(recover)");
        }

        public String Say(String message)
        {
            /* Broadcasts message to all clients. 
             * Message format: length < say_coach_msg_size(see Section B.1) and it must consist of alphanumeric characters and / or the symbols().+ /?*<>_ */
            return this.serverConnection.Send("(say " + message + ")");
        }

        public String ChangePlayerType(String teamName, int playerNumber, int playerType)
        {
            /* playerType is a digit between 0 and 6, where 0 denotes the default player type. */
            return this.serverConnection.Send("(change_player_type " + teamName + " " + playerNumber.ToString() + " " + playerType.ToString() + ")");
        }

#endregion

    }
}
