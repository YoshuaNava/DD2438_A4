﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamZlatan.BehaviorTrees;
using System.Timers;
using TeamZlatan.PlayingUtils;
using System.Threading;

namespace TeamZlatan.Agents
{
    class Team
    {

        #region Attributes
        /* Structures to keep track of all the players, and ball, in the field */
        private String teamName;
        private int N_players = 11;
        private char teamSide;
        private List<Player> teamPlayers = new List<Player>();
        private Coach teamCoach;
        private float[,] oppositeTeamPositions = null;
        private float[] oppositeGoaliePosition = null;
        private Ball gameBall;
        private Field gameField;

        private int triedCatchAtCycle = -1;

        /* Network parameters */
        private int serverPort = 6000;          // player port
        private int offlineCoachPort = 6001;    // trainer/offline coach port
        private int onlineCoachPort = 6002;    // online coach port

        /* Coordinates of the center of the goals of each team */
        private float[] opponentGoal = { 52.5F, 0.0F };
        private float[] ownGoal = { -52.5F, 0.0F };

        /* Game score */
        private int goalsInFavour = 0;
        private int goalsAgainst = 0;
        private int timeTranscurred;

        /* Game mode */
        private String currentMode = "before_kick_off"; // current play mode
        private char kickOffSide = 'l';
        private int cycle = 0;

        private BehaviorTree bTree;

        private SupportSpotCalculator sspotCalculator;

        private TikiTakaSpotCalculator tikitakaSpotCalculator;

        private float[] goalieBlockingPosition;

        /* The goalie */
        private Player goalie;

        /* Player that will run into the ball to recover it */
        private Player closestToBallPlayer;

        /* Players that will span around the field to receive a pass */
        private List<Player> receivingPlayers;

        /* Player that has the ball */
        private Player lastBallControllingPlayer;
        private Player ballControllingPlayer;

        /* Player that aims to make a run behind the defenders, looking for a through pass */
        private Player supportingPlayer;

        /* Enemy players assigned to each of our players for marking */
        Dictionary<Player, int> markAssigned;

        private FieldPlotWindow fieldPlotWindow;
        private DateTime time = new DateTime(1, 1, 1);

        public float[] blockingInfo = null;
        #endregion


        #region GettersSetters
        public string TeamName { get { return teamName; } set { teamName = value; } }
        public int GoalsInFavour { get { return goalsInFavour; } set { goalsInFavour = value; } }
        public int GoalsAgainst { get { return goalsAgainst; } set { goalsAgainst = value; } }
        public int TimeTranscurred { get { return timeTranscurred; } set { timeTranscurred = value; } }
        public BehaviorTree BTree { get { return bTree; } set { bTree = value; } }
        public List<Player> TeamPlayers { get { return teamPlayers; } set { teamPlayers = value; } }
        public Coach TeamCoach { get { return teamCoach; } set { teamCoach = value; } }
        public FieldPlotWindow FieldPlotWindow { get { return fieldPlotWindow; } set { fieldPlotWindow = value; } }
        public DateTime Time { get { return time; } set { time = value; } }
        public float[,] OppositeTeamPositions { get { return oppositeTeamPositions; } set { oppositeTeamPositions = value; } }
        public char TeamSide { get { return teamSide; } set { teamSide = value; } }
        public char KickOffSide { get { return kickOffSide; } set { kickOffSide = value; } }
        public float[] OpponentGoal { get { return opponentGoal; } set { opponentGoal = value; } }
        public float[] OwnGoal { get { return ownGoal; } set { ownGoal = value; } }
        public String CurrentMode { get { return currentMode; } set { currentMode = value; } }
        public SupportSpotCalculator SspotCalculator { get { return sspotCalculator; } set { sspotCalculator = value; } }
        public List<Player> ReceivingPlayers { get { return receivingPlayers; } set { receivingPlayers = value; } }
        public Player Goalie { get { return goalie; } set { goalie = value; } }
        public Player ClosestToBallPlayer { get { return closestToBallPlayer; } set { closestToBallPlayer = value; } }
        public Player BallControllingPlayer { get { return ballControllingPlayer; } set { ballControllingPlayer = value; } }
        public Player SupportingPlayer { get { return supportingPlayer; } set { supportingPlayer = value; } }
        public Ball GameBall { get { return gameBall; } set { gameBall = value; } }
        public Field GameField { get { return gameField; } set { gameField = value; } }
        public int Cycle { get { return cycle; } }
        public int TriedCatchAtCycle { get { return triedCatchAtCycle; } set { triedCatchAtCycle = value; } }
        public TikiTakaSpotCalculator TikitakaSpotCalculator { get { return tikitakaSpotCalculator; } set { tikitakaSpotCalculator = value; } }
        public float[] GoalieBlockingPosition { get { return goalieBlockingPosition; } set { goalieBlockingPosition = value; } }
        public Dictionary<Player, int> MarkAssigned { get { return markAssigned; } set { markAssigned = value; } }
        #endregion


        public Team(String teamName)
        {
            this.teamName = teamName;
            teamPlayers.Add(new Player("goalie", teamName, serverPort));
            teamPlayers.Add(new Player("rightBack", teamName, serverPort));
            teamPlayers.Add(new Player("centreBack", teamName, serverPort));
            teamPlayers.Add(new Player("centreBack", teamName, serverPort));
            teamPlayers.Add(new Player("leftBack", teamName, serverPort));
            teamPlayers.Add(new Player("midfielder", teamName, serverPort));
            teamPlayers.Add(new Player("midfielder", teamName, serverPort));
            teamPlayers.Add(new Player("midfielder", teamName, serverPort));
            teamPlayers.Add(new Player("rightWinger", teamName, serverPort));
            teamPlayers.Add(new Player("leftWinger", teamName, serverPort));
            teamPlayers.Add(new Player("striker", teamName, serverPort));

            for (int i = 0; i < N_players; i++)
            {
                teamPlayers[i].Team = this;


            }

            teamSide = teamPlayers[0].TeamSide;

            goalie = teamPlayers[0];
            gameBall = new Ball();
            gameField = new Field(teamSide);
            if (teamSide == 'r')
            {
                opponentGoal[0] = opponentGoal[0] * -1;
                ownGoal[0] = ownGoal[0] * -1;
            }

            
            teamCoach = new Coach(teamName, offlineCoachPort, onlineCoachPort, online:true);
            teamCoach.EarOn();

            //testing
            //for (int i = 0; i < N_players; i++)
            //    teamCoach.ChangePlayerType(teamName, i, 3);

            sspotCalculator = new SupportSpotCalculator(teamSide);

            InitializeBehaviorTree();
            AskServerAboutGameStatus();
        }


        public void InitializeBehaviorTree()
        {
            bTree = new BehaviorTree();

            /* Before kick off & kick off nodes */
            TreeNode beforeKickOffSequence = bTree.Root.Child = new SequenceNode("BeforeKickOffSequence");
            (beforeKickOffSequence as SequenceNode).Children = new TreeNode[]
            {
                new IsBeforeKickOff(),
                new TeamPrepareForKickOff(),
                new IsTeamReady(),
                //new IsOpponentReady(),
                new KickOffGame()
            };

            TreeNode kickOffSequence = bTree.Root.Child = new SequenceNode("KickOffSequence");
            (kickOffSequence as SequenceNode).Children = new TreeNode[]
            {
                new IsKickOff(),
                new DoNothing()
            };

            /* Defend nodes */
            TreeNode defendSequence = new SequenceNode("DefendSequence");
            (defendSequence as SequenceNode).Children = new TreeNode[]
            {
                new IsBallOnTeamSide(),
                new SetDefensiveFormation()
            };

            TreeNode pressingSequence = new SequenceNode("PressingSequence");
            (pressingSequence as SequenceNode).Children = new TreeNode[]
            {
                new IsBallOnEnemySide(),
                new SetPressingFormation()
            };

            TreeNode defenseSelector = new SelectorNode("DefenseSelector");
            (defenseSelector as SelectorNode).Children = new TreeNode[]
            {
                defendSequence,
                pressingSequence
            };

            TreeNode defenseSequence = new SequenceNode("DefenseSequence");
            (defenseSequence as SequenceNode).Children = new TreeNode[]
            {
                new IsPlayOn(),
                new DoesEnemyHaveBall(),
                defenseSelector
            };

            /* Attack nodes */
            TreeNode attackSequence = new SequenceNode("AttackSelector");
            (attackSequence as SequenceNode).Children = new TreeNode[]
            {
                new IsPlayOn(),
                //new DoesOurTeamHaveBall(),
                new SetOffensiveFormation()
            };


            /* Root node */
            TreeNode rootSelector = bTree.Root.Child = new SelectorNode("TeamRootSelector");
            (rootSelector as SelectorNode).Children = new TreeNode[]
            {
                beforeKickOffSequence,
                kickOffSequence,
                defenseSequence,
                attackSequence
            };
        }


        public void RunTeamBehaviorTree()
        {
            AskServerAboutGameStatus();
            AssignMarksToPlayers();

            gameBall.PredPosition = null;
            gameBall.PredictStoppingPosition();

            if (ballControllingPlayer != null)
            {
                lastBallControllingPlayer = ballControllingPlayer;
            }
            else
            {
                if(lastBallControllingPlayer != null)
                {
                    ballControllingPlayer = lastBallControllingPlayer;
                }
            }


            FindPlayersOffisde();

            //if (Controllers.EuclideanNorm(gameBall.Velocity[0], gameBall.Velocity[1]) > 0)
            //{
            //    currentMode = "play_on"; // could be other modes!
            //}


            ballControllingPlayer = EstimateBallControllingPlayer();
            
            supportingPlayer = null;
            receivingPlayers = null;

            if ((Controllers.EuclideanNorm(gameBall.Velocity[0], gameBall.Velocity[1]) > Controllers.ballControlVel) || (ballControllingPlayer != null))
                closestToBallPlayer = null;


            if ((ballControllingPlayer == null) && (Controllers.EuclideanNorm(gameBall.Velocity[0], gameBall.Velocity[1]) <= Controllers.ballControlVel))
            {
                Player newClosestToBallPlayer = EstimateClosestPlayerToBall();
                
                if (closestToBallPlayer == null)
                {
                    closestToBallPlayer = newClosestToBallPlayer;
                }
                else
                {
                    if (Controllers.EuclideanDistance(closestToBallPlayer.Pose[0], closestToBallPlayer.Pose[1], gameBall.Position[0], gameBall.Position[1]) > 0.1 + Controllers.EuclideanDistance(newClosestToBallPlayer.Pose[0], newClosestToBallPlayer.Pose[1], gameBall.Position[0], gameBall.Position[1]))
                    {
                        closestToBallPlayer = newClosestToBallPlayer;
                    }
                }
            }
            else
            {
                sspotCalculator = new SupportSpotCalculator(teamSide);
                sspotCalculator.DetermineBestSupportSpot(this);
                supportingPlayer = EstimateSupportingPlayer();

                EstimateReceivingPlayers();

                tikitakaSpotCalculator = new TikiTakaSpotCalculator(this.receivingPlayers);
                tikitakaSpotCalculator.DetermineBestTikiTakaSpot(this);
            }
            UpdateGoalieBlockingPosition();

            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>();
            data["teamPlayers"] = this.teamPlayers;
            data["coach"] = this.teamCoach;
            data["team"] = this;

            bTree.Update(data);
        }


        public void RunPlayersBehaviorTrees()
        {
            for (int i = 0; i < N_players; i++)
            {
                teamPlayers[i].Team = this;
                //teamPlayers[i].RunBehaviorTree();
            }

            //Parallel.ForEach(teamPlayers, obj => obj.Team = this);
            Parallel.ForEach(teamPlayers, obj => obj.RunBehaviorTree()); // by usin this, detecting play modes works correctly!
        }


        public Player EstimateClosestPlayerToBall()
        {
            Player closestToBallPlayer = null;
            foreach (Player p in teamPlayers)
            {
                if (closestToBallPlayer == null)
                {
                    if (p.IsOffside == false)
                        closestToBallPlayer = p;
                }
                else
                {
                    float playerBallDist = Controllers.EuclideanDistance(p.Pose[0], p.Pose[1], gameBall.PredPosition[0], gameBall.PredPosition[1]);
                    float closestPlayerBallDist = Controllers.EuclideanDistance(closestToBallPlayer.Pose[0], closestToBallPlayer.Pose[1], gameBall.PredPosition[0], gameBall.PredPosition[1]);
                    if ((playerBallDist + 0.5 < closestPlayerBallDist) && (p.IsOffside == false))
                    {
                        closestToBallPlayer = p;
                    }
                    else
                    {
                        closestToBallPlayer.GoalPosition = closestToBallPlayer.PositionInFormation;
                        p.GoalPosition = p.PositionInFormation;
                    }
                }
            }
            closestToBallPlayer.GoalPosition[0] = gameBall.PredPosition[0];
            closestToBallPlayer.GoalPosition[1] = gameBall.PredPosition[1];

            return closestToBallPlayer;
        }

        public Player EstimateBallControllingPlayer()
        {
            Player ballControllingPlayer = null;
            foreach (Player p in teamPlayers)
            {
                float playerBallDist = Controllers.EuclideanDistance(p.Pose[0], p.Pose[1], gameBall.Position[0], gameBall.Position[1]);
                if (ballControllingPlayer == null)
                {
                    if ((playerBallDist <= Controllers.ballControlDistance) && (p.IsOffside == false))
                    {
                        ballControllingPlayer = p;
                    }
                }
                else
                {
                    float ballControllingPlayerDist = Controllers.EuclideanDistance(ballControllingPlayer.Pose[0], ballControllingPlayer.Pose[1], gameBall.Position[0], gameBall.Position[1]);
                    if ((playerBallDist < ballControllingPlayerDist) && (p.IsOffside == false))
                    {
                        ballControllingPlayer = p;
                    }
                    else
                    {
                        ballControllingPlayer.GoalPosition = ballControllingPlayer.PositionInFormation;
                        p.GoalPosition = p.PositionInFormation;
                    }
                }
            }

            //if(ballControllingPlayer != null)
            //    ballControllingPlayer.GoalPosition = ballControllingPlayer.PositionInFormation;

            return ballControllingPlayer;
        }

        public Player EstimateSupportingPlayer()
        {
            Player supportingPlayer = null;
            foreach (Player p in teamPlayers)
            {
                if (supportingPlayer == null)
                {
                    if (p.IsOffside == false)
                        supportingPlayer = p;
                }
                else
                {
                    float playerSpotDist = Controllers.EuclideanDistance(p.Pose[0], p.Pose[1], sspotCalculator.SecondSupportSpot.SpotPos[0], sspotCalculator.SecondSupportSpot.SpotPos[1]);
                    float supportingPlayerSpotDist = Controllers.EuclideanDistance(supportingPlayer.Pose[0], supportingPlayer.Pose[1], sspotCalculator.SecondSupportSpot.SpotPos[0], sspotCalculator.SecondSupportSpot.SpotPos[1]);

                    if ((playerSpotDist < supportingPlayerSpotDist) && (p != ballControllingPlayer) && (p.IsOffside == false))
                    {
                        supportingPlayer.GoalPosition = supportingPlayer.PositionInFormation;
                        supportingPlayer = p;
                    }
                    else
                    {
                        p.GoalPosition = p.PositionInFormation;
                    }
                }
            }
            supportingPlayer.GoalPosition[0] = sspotCalculator.SecondSupportSpot.SpotPos[0];
            supportingPlayer.GoalPosition[1] = sspotCalculator.SecondSupportSpot.SpotPos[1];

            return supportingPlayer;
        }

        public void EstimateReceivingPlayers()
        {
            receivingPlayers = new List<Player>();
            foreach (Player p in teamPlayers)
            {
                if ((p != ballControllingPlayer) && (p != supportingPlayer))
                {
                    if (FeasiblePass(p))
                    {
                        p.GoalPosition = p.PositionInFormation;
                        receivingPlayers.Add(p);
                    }
                }
            }
            if(receivingPlayers.Count == 0)
            {
                receivingPlayers = null;
            }
        }


        public bool FeasiblePass(Player receiver)
        {
            if (oppositeTeamPositions == null)
            {
                if (receiver.IsOffside)
                {
                    return false;
                }
            }
            else
            {
                for (int i = 0; i < oppositeTeamPositions.GetLength(1); i++)
                {
                    float[] opponentPos = { oppositeTeamPositions[0, i], oppositeTeamPositions[1, i] };
                    if ((gameBall.RiskOfInterception(receiver.Pose, opponentPos, Controllers.passInterceptionThresh) == true) || (receiver.IsOffside == true))
                    {
                        //Console.WriteLine("Ño");
                        return false;
                    }
                }
            }

            return true;
        }

        public void FindPlayersOffisde()
        {
            if (this.oppositeTeamPositions == null)
            {
                if (this.teamSide == 'l')
                {
                    foreach (Player p in this.teamPlayers)
                    {
                        if (p == this.lastBallControllingPlayer)
                        {
                            p.IsOffside = false;
                            continue;
                        }
                        else
                        {

                            if ((p.Pose[0] > 0.0) && (p.Pose[0] > gameBall.Position[0]))
                            {
                                //Console.WriteLine("OFFSIDE !!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                                p.IsOffside = true;
                            }
                            else
                            {
                                //if (p.Pose[0] <= gameBall.Position[0])
                                //{
                                //    if (lastBallControllingPlayer != null)
                                //        p.IsOffside = true;
                                //}
                                //else
                                    p.IsOffside = false;
                            }
                        }
                    }
                }
                else
                {
                    foreach (Player p in this.teamPlayers)
                    {
                        if (p == this.lastBallControllingPlayer)
                        {
                            p.IsOffside = false;
                            continue;
                        }
                        else
                        {

                            if ((p.Pose[0] < 0.0) && (p.Pose[0] < gameBall.Position[0]))
                            {
                                //Console.WriteLine("OFFSIDE !!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                                p.IsOffside = true;
                            }
                            else
                            {
                                //if (p.Pose[0] >= gameBall.Position[0])
                                //{
                                //    if (lastBallControllingPlayer != null)
                                //        p.IsOffside = true;
                                //}
                                //else
                                    p.IsOffside = false;
                            }
                        }
                    }
                }
            }
            else
            {
                if (this.teamSide == 'l')
                {
                    float deepestDefenderX = 0;
                    if (this.oppositeTeamPositions != null)
                    {
                        for (int i = 1; i < this.oppositeTeamPositions.GetLength(1); i++)
                        {
                            if (this.oppositeTeamPositions[0, i] > deepestDefenderX)
                            {
                                deepestDefenderX = this.oppositeTeamPositions[0, i];
                            }
                        }

                    }

                    if(gameBall.Position[0] > deepestDefenderX)
                    {
                        deepestDefenderX = gameBall.Position[0];
                    }

                    foreach (Player p in this.teamPlayers)
                    {
                        if (p == this.lastBallControllingPlayer)
                        {
                            p.IsOffside = false;
                            continue;
                        }
                        else
                        {
                            if (p.Pose[0] > deepestDefenderX)
                            {
                                p.IsOffside = true;
                            }
                            else
                            {
                                p.IsOffside = false;
                            }
                        }
                    }
                }
                else
                {
                    float deepestDefenderX = 0;
                    if (this.oppositeTeamPositions != null)
                    {
                        for (int i = 1; i < this.oppositeTeamPositions.GetLength(1); i++)
                        {
                            if (this.oppositeTeamPositions[0, i] < deepestDefenderX)
                            {
                                deepestDefenderX = this.oppositeTeamPositions[0, i];
                            }
                        }

                    }

                    if (gameBall.Position[0] < deepestDefenderX)
                    {
                        deepestDefenderX = gameBall.Position[0];
                    }

                    foreach (Player p in this.teamPlayers)
                    {
                        if (p == this.lastBallControllingPlayer)
                        {
                            p.IsOffside = false;
                            continue;
                        }
                        else
                        {
                            if (p.Pose[0] < deepestDefenderX)
                            {
                                p.IsOffside = true;
                            }
                            else
                            {
                                p.IsOffside = false;
                            }
                        }
                    }
                }
            }
        }

        public void AssignMarksToPlayers()
        {
            markAssigned = new Dictionary<Player, int>();
            if (oppositeTeamPositions != null)
            {
                HashSet<int> enemiesMarked = new HashSet<int>();
                int minDistIdx = 1;
                float minDist = 500;
                float dist;

                foreach (Player p in TeamPlayers)
                {
                    if (p != goalie)
                    {
                        minDistIdx = 1;
                        minDist = 500;
                        for (int i = 1; i < oppositeTeamPositions.GetLength(1); i++)
                        {
                            if (enemiesMarked.Contains(i) == false)
                            {
                                dist = Controllers.EuclideanDistance(p.Pose[0], p.Pose[1], oppositeTeamPositions[0, i], oppositeTeamPositions[1, i]);
                                if (dist < minDist)
                                {
                                    minDistIdx = i;
                                    minDist = dist;
                                }
                            }
                        }
                        enemiesMarked.Add(minDistIdx);
                        markAssigned[p] = minDistIdx;
                    }
                    else
                    {
                        markAssigned[p] = -1;
                    }
                }
            }
            else
            {
                foreach (Player p in TeamPlayers)
                {
                    markAssigned[p] = -1;
                }
            }
        }

        public void UpdateGoalieBlockingPosition()
        {
            float[] ballPosition = gameBall.Position;
            if (currentMode == "goal_kick" && kickOffSide == teamSide || currentMode == "free_kick" && kickOffSide == teamSide)
            {
                goalieBlockingPosition = ballPosition; // grab ball!
                blockingInfo = null;
                return;
            }
            ballPosition = gameBall.PredPosition;
            float[] ballVelocity = gameBall.Velocity;
            float[] pos = { 0.0F, teamPlayers[0].Pose[1] }; // x position in left side coordinates
            if (teamPlayers[0].Role == "Attack")
                pos[0] = -40.0F;
            else if (teamPlayers[0].Role == "Defend")
                pos[0] = -50.0F;
            else
                pos[0] = -50.0F;

            Field field = new Field(teamSide);

            float xPos = pos[0];
            if (teamSide == 'r')
                xPos = -xPos;

            float y1 = xPos * (ballPosition[1] - field.GoalBounds[3]) / (ballPosition[0] - field.GoalBounds[0]) + field.GoalBounds[3] - (ballPosition[1] - field.GoalBounds[3]) / (ballPosition[0] - field.GoalBounds[0]) * field.GoalBounds[0];
            float y2 = xPos * (ballPosition[1] - field.GoalBounds[2]) / (ballPosition[0] - field.GoalBounds[0]) + field.GoalBounds[2] - (ballPosition[1] - field.GoalBounds[2]) / (ballPosition[0] - field.GoalBounds[0]) * field.GoalBounds[0];
            float y_blocking = (y1 + y2) / 2;
            blockingInfo = new float[3] { xPos, y1, y2 };

            // if possible, choose a better position between y1 & y2
            float y_intercept;
            if (teamSide == 'l' && ballVelocity[0] <= 0 || teamSide == 'r' && ballVelocity[0] >= 0)
            {
                y_intercept = field.GoalBounds[0] * (ballVelocity[1] / ballVelocity[0]);
                if (y_intercept > field.GoalBounds[2] && y_intercept < field.GoalBounds[3])
                {
                    y_blocking = xPos * (ballPosition[1] - y_intercept) / (ballPosition[0] - field.GoalBounds[0]) + y_intercept - (ballPosition[1] - y_intercept) / (ballPosition[0] - field.GoalBounds[0]) * field.GoalBounds[0];
                    blockingInfo = new float[4] { xPos, y1, y2, y_intercept };
                }
            }

            if (teamSide == 'r')
                y_blocking = -y_blocking; //y directin is also flipped!
            pos[1] = y_blocking;
            goalieBlockingPosition = pos;
        }

        public void AskServerAboutGameStatus()
        {
            String serverReply;

            while (teamCoach.IncomingMessage())
            {
                serverReply = this.teamCoach.Hear();
                ParseMessage(serverReply);
                //Console.WriteLine("HEARING MESSAGE" + hearReply);
                //Console.WriteLine("ok bye");
            }

            serverReply = this.teamCoach.LookAround();
            serverReply = this.teamCoach.Hear();
            ParseMessage(serverReply);
            //Console.WriteLine("Play mode " + currentMode);
        }

        public void AskServerAboutGameScore()
        {
            String reply = teamCoach.ServerConnection.Send("(score)");
            ParseMessage(reply);
        }


        public void LeaveGame()
        {
            foreach (Player player in teamPlayers)
            {
                player.Bye();
            }
        }

        #region MessageParsing
        public void ParseMessage(String message, bool onlyhear = false)
        {
            if ((message == null) || (message.Length < 5))
                return;

            if (onlyhear && message.Substring(1, 4) != "hear" && !message.Contains("referee"))
                return;

            if (message.Contains("Error") || message.Contains("Warning"))
                return;

            if (message.Substring(1, 4) == "hear")
            {
                message = message.Substring(0, message.Length - 1);
                //Console.WriteLine("|" + message + "|");
                if (message.Contains("kick_off_l"))
                {
                    this.kickOffSide = 'l';
                    this.currentMode = "kick_off";
                }
                else if (message.Contains("kick_off_r"))
                {
                    this.kickOffSide = 'r';
                    this.currentMode = "kick_off";
                }
                else if (message.Contains("play_on"))
                {
                    //Console.WriteLine("HEARING MESSAGE" + message);
                    this.currentMode = "play_on";
                }
                else if (message.Contains("goal_l_"))
                {
                    this.kickOffSide = 'r';
                    this.currentMode = "before_kick_off";
                }
                else if (message.Contains("goal_r_"))
                {
                    this.kickOffSide = 'l';
                    this.currentMode = "before_kick_off";
                }
                else if (message.Contains("free_kick_l"))
                {
                    this.kickOffSide = 'l';
                    this.currentMode = "free_kick";
                }
                else if (message.Contains("free_kick_r"))
                {
                    this.kickOffSide = 'r';
                    this.currentMode = "free_kick";
                }
                else if (message.Contains("goalie_catch_ball_l"))
                {
                    this.kickOffSide = 'l';
                    this.currentMode = "goalie_catch_ball";
                }
                else if (message.Contains("goalie_catch_ball_r"))
                {
                    this.kickOffSide = 'r';
                    this.currentMode = "goalie_catch_ball";
                }
                else if (message.Contains("goal_kick"))
                {
                    this.currentMode = "goal_kick";
                }
                else if (message.Contains("drop_ball"))
                {
                    this.ballControllingPlayer = null;
                    this.closestToBallPlayer = null;
                    this.supportingPlayer = null;
                    this.receivingPlayers = null;
                    this.currentMode = "play_on";
                }
                else if (message.Contains("corner_kick"))
                {
                    this.currentMode = "corner_kick";
                }
                else if (message.Contains("kick_in"))
                {
                    //this.currentMode = "kick_in";
                    this.currentMode = "game_on"; //TOCO: check if it works??
                }

                //Console.WriteLine("|" + message + "|");
                //Console.ReadKey();
            }
            else if (message.Substring(1, 5) == "score")
            {
                int numDigits = 1;
                message = message.Substring(6, message.Length - 6);
                //Console.WriteLine("message: " + message);
                for (int i=0; i<4 ;i++)
                {
                    int num;
                    if(int.TryParse(message.Substring(0, 1+i), out num))
                    {
                        numDigits++;
                    }
                }
                int.TryParse(message.Substring(0, 1 + numDigits), out this.timeTranscurred);
                //Console.WriteLine("Time transcurred = " + this.timeTranscurred);
                message = message.Substring(numDigits, message.Length - numDigits);
            }
            else if (message.Substring(1, 7) == "ok look")
            {
                //Console.WriteLine("-- ok look --");

                // player format: ((p "TeamZlatan" 1 goalie) -38.6975 26.9836 0.0131324 -0.00120259 0 0) = ((p "TeamZlatan" 1 goalie) X Y deltaX deltaY bodyAngle neckAngle)

                List<String> messageList = message.Split('(', ')').ToList();

                int playerNR = 0;
                int playerNROpposite = 0;

                if (messageList.Count > 0)
                {
                    int L = messageList[1].Length;
                    int nextCycle = int.Parse(messageList[1].Substring(8, (L - 8)));
                    //if (nextCycle > cycle && currentMode == "before_kick_off")
                    //    currentMode = "kick_off";
                    cycle = nextCycle;
                }

                for (int i = 0; i < messageList.Count; i++)
                {
                    if (messageList[i].Length > 0 && messageList[i].Substring(0, 1) == "b")
                    {
                        // next element is player info
                        String[] playerInfoList = messageList[i + 1].Split(' ');
                        gameBall.Position[0] = float.Parse(playerInfoList[1]);
                        gameBall.Position[1] = float.Parse(playerInfoList[2]);
                        gameBall.Velocity[0] = float.Parse(playerInfoList[3]);
                        gameBall.Velocity[1] = float.Parse(playerInfoList[4]);
                    }
                    if (messageList[i].Length > 0 && messageList[i].Substring(0, 1) == "p")
                    {
                        String[] playerInfoList = messageList[i + 1].Split(' ');
                        bool oppositeTeam = true;
                        if (messageList[i].Length >= (teamName.Length + 3)) // own team
                        {
                            if (messageList[i].Substring(3, teamName.Length) == teamName)
                            {
                                teamPlayers[playerNR].Pose[0] = float.Parse(playerInfoList[1]);
                                teamPlayers[playerNR].Pose[1] = float.Parse(playerInfoList[2]);
                                teamPlayers[playerNR].Velocity[0] = float.Parse(playerInfoList[3]);
                                teamPlayers[playerNR].Velocity[1] = float.Parse(playerInfoList[4]);
                                teamPlayers[playerNR].Pose[2] = float.Parse(playerInfoList[5]);

                                playerNR++;
                                oppositeTeam = false;

                            }
                        }
                        
                        if(oppositeTeam)
                        {
                            if (oppositeTeamPositions == null)
                                oppositeTeamPositions = new float[3, 11];
                            oppositeTeamPositions[0, playerNROpposite] = float.Parse(playerInfoList[1]);
                            oppositeTeamPositions[1, playerNROpposite] = float.Parse(playerInfoList[2]);
                            oppositeTeamPositions[2, playerNROpposite] = float.Parse(playerInfoList[5]);

                            //if (messageList[i].Contains("goalie"))
                            //{
                            //    oppositeGoaliePosition = new float[3];
                            //    oppositeGoaliePosition[0] = oppositeTeamPositions[0, playerNROpposite];
                            //    oppositeGoaliePosition[1] = oppositeTeamPositions[1, playerNROpposite];
                            //    oppositeGoaliePosition[2] = oppositeTeamPositions[2, playerNROpposite];
                            //    oppositeGoaliePosition = oppositeTeamPositions[, playerNROpposite];
                            //}
                            playerNROpposite++;
                        }
                    }
                }
                //Console.WriteLine("|" + message + "|");
                //Console.ReadKey();
            }
            else if (message.Length > 10 && message.Substring(1, 10) == "see_global")
            {
                //Console.WriteLine("-- see global --");
                //Console.WriteLine("|" + message + "|");
                // player format: ((p "TeamZlatan" 1 goalie) -38.6975 26.9836 0.0131324 -0.00120259 0 0) = ((p "TeamZlatan" 1 goalie) X Y deltaX deltaY bodyAngle neckAngle)

                List<String> messageList = message.Split('(', ')').ToList();
                int playerNR = 0;
                int playerNROpposite = 0;
                for (int i = 0; i < messageList.Count; i++)
                {
                    if (messageList[i].Length > 0 && messageList[i].Substring(0, 1) == "b")
                    {
                        // next element is player info
                        String[] playerInfoList = messageList[i + 1].Split(' ');
                        gameBall.Position[0] = float.Parse(playerInfoList[1]);
                        gameBall.Position[1] = float.Parse(playerInfoList[2]);
                        gameBall.Velocity[0] = float.Parse(playerInfoList[3]);
                        gameBall.Velocity[1] = float.Parse(playerInfoList[4]);
                    }
                    if (messageList[i].Length > 0 && messageList[i].Substring(0, 1) == "p")
                    {
                        String[] playerInfoList = messageList[i + 1].Split(' ');
                        bool oppositeTeam = true;
                        if (messageList[i].Length >= (teamName.Length + 3)) // own team
                        {
                            if (messageList[i].Substring(3, teamName.Length) == teamName)
                            {
                                teamPlayers[playerNR].Pose[0] = float.Parse(playerInfoList[1]);
                                teamPlayers[playerNR].Pose[1] = float.Parse(playerInfoList[2]);
                                teamPlayers[playerNR].Velocity[0] = float.Parse(playerInfoList[3]);
                                teamPlayers[playerNR].Velocity[1] = float.Parse(playerInfoList[4]);
                                teamPlayers[playerNR].Pose[2] = float.Parse(playerInfoList[5]);

                                playerNR++;
                                oppositeTeam = false;
                            }
                        }

                        if(oppositeTeam) // opponents team
                        {
                            if (oppositeTeamPositions == null)
                                oppositeTeamPositions = new float[3, 11];
                            oppositeTeamPositions[0, playerNROpposite] = float.Parse(playerInfoList[1]);
                            oppositeTeamPositions[1, playerNROpposite] = float.Parse(playerInfoList[2]);
                            oppositeTeamPositions[2, playerNROpposite] = float.Parse(playerInfoList[5]);
                            playerNROpposite++;
                        }
                    }
                }
            }
            //Console.WriteLine("|" + message + "|");
            //Console.ReadKey();
        }
        #endregion
    }
}
